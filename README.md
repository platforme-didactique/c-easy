<div align="center">
    <img src="web/files/lib/logo.png" alt="C-Easy" width="250"/>
</div>

# C-Easy

_C-Easy_ est une platerforme didactique d'apprentissage du langage C. _C-Easy_ a été dévéloppé dans le cadre d'un projet de 4ème année d'écoles d'ingénieurs par [Jonathan Baudillon](https://gitlab.com/Glorgs), [Gabrielle Chastaing](https://gitlab.com/DemmMy), [Adam Colas](https://gitlab.com/fatevald), [Sylvain Huss](https://gitlab.com/sylvainhuss), [Loïc Mayol](https://gitlab.com/DO3B) et [Marius Ridel](https://gitlab.com/Marius_RIDEL).

Vous pourrez retrouver toutes les informations pour installer et utiliser l'application sur notre [Wiki](https://gitlab.com/platforme-didactique/c-easy/wikis/home).