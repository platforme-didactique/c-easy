-- MySQL dump 10.13  Distrib 8.0.15, for Linux (x86_64)
--
-- Host: localhost    Database: bd
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
	
DROP DATABASE IF EXISTS bd;
CREATE DATABASE bd;

CREATE USER IF NOT EXISTS 'web'@'172.58.0.2' IDENTIFIED BY 'mdp';

GRANT ALL ON bd.* TO 'web'@'172.58.0.2';

USE bd;

--
-- Table structure for table `chapitre`
--

DROP TABLE IF EXISTS `chapitre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `chapitre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `indice` text DEFAULT NULL,
  `visible` int(1) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chapitre`
--

--
-- Table structure for table `exercice`
--

DROP TABLE IF EXISTS `exercice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exercice` (
  `id_chap` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `consigne` text DEFAULT NULL,
  `corrige` text DEFAULT NULL,
  `dependances` text DEFAULT NULL,
  `main` text DEFAULT NULL,
  `visible` int(1) DEFAULT 1,
  PRIMARY KEY (`id_chap`,`id`),
  KEY(`id_chap`),
  KEY(`id`),
  CONSTRAINT `exercice_ibfk_1` FOREIGN KEY (`id_chap`) REFERENCES `chapitre` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exercice`
--


--
-- Table structure for table `jeutest`
--

DROP TABLE IF EXISTS `jeutest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `jeutest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_chap` int(11) NOT NULL,
  `id_ex` int(11) NOT NULL,
  `entree` varchar(255) DEFAULT NULL,
  `sortie` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jeutest_ibfk_1` (`id_ex`),
  KEY `jeutest_ibfk_2` (`id_chap`),
  CONSTRAINT `jeutest_ibfk_1` FOREIGN KEY (`id_ex`) REFERENCES `exercice` (`id`) ON DELETE CASCADE,
  CONSTRAINT `jeutest_ibfk_2` FOREIGN KEY (`id_chap`) REFERENCES `chapitre` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jeutest`
--

--
-- Table structure for table `promo`
--

DROP TABLE IF EXISTS `promo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `promo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `annee` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promo`
--

--
-- Table structure for table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `type` int(11) DEFAULT '1',
  `promo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `utilisateur_ibfk_1` FOREIGN KEY (`promo`) REFERENCES `promo` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilisateur`
--

LOCK TABLES `utilisateur` WRITE;
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` VALUES (1,'administrateur.ceasy@univ-amu.fr','ba6ad89abc7caf5787589aed4b0cc43c2fa50241',0, NULL);
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `realise`
--

DROP TABLE IF EXISTS `realise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `realise` (
  `id_ex` int(11) NOT NULL,
  `id_chap` int(11) NOT NULL,
  `id_eleve` int(11) NOT NULL,
  `temps_debut` datetime DEFAULT NULL,
  `temps_valide` datetime DEFAULT NULL,
  `nb_essai` int(11) DEFAULT '0',
  `valide` int(11) DEFAULT '0',
  `code` text DEFAULT NULL,
  PRIMARY KEY (`id_chap`,`id_ex`,`id_eleve`),
  KEY `id_eleve` (`id_eleve`),
  KEY `realise_ibfk_2` (`id_ex`),
  CONSTRAINT `realise_ibfk_1` FOREIGN KEY (`id_chap`) REFERENCES `chapitre` (`id`) ON DELETE CASCADE,
  CONSTRAINT `realise_ibfk_2` FOREIGN KEY (`id_ex`) REFERENCES `exercice` (`id`) ON DELETE CASCADE,
  CONSTRAINT `realise_ibfk_3` FOREIGN KEY (`id_eleve`) REFERENCES `utilisateur` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `realise`
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-06 11:49:52

DELIMITER $$
CREATE TRIGGER lesBeauxExos BEFORE INSERT ON exercice
FOR EACH ROW BEGIN
    SET NEW.id = (
       SELECT IFNULL(MAX(id), 0) + 1
       FROM exercice
       WHERE id_chap = NEW.id_chap
    );
END $$

CREATE TRIGGER updateExo BEFORE UPDATE ON exercice
FOR EACH ROW BEGIN
  IF (OLD.id_chap <> NEW.id_chap) THEN
      SET NEW.id = (
        SELECT IFNULL(MAX(id), 0) + 1
        FROM exercice
        WHERE id_chap = NEW.id_chap
      );
  	  UPDATE jeutest SET id_chap = NEW.id_chap, id_ex = NEW.id WHERE id_chap = OLD.id_chap AND id_ex = OLD.id;
      UPDATE realise SET id_chap = NEW.id_chap, id_ex = NEW.id WHERE id_chap = OLD.id_chap AND id_ex = OLD.id;
  END IF;
END $$
DELIMITER ;