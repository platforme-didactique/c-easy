<?php
    /**
     * Classe JeuTest
     * Classe définissant l'objet JeuTest et les fonctions associées pour traîter l'objet en base de données.
     */
    class JeuTest {
        public function __construct($id, $id_chap, $id_ex, $entree, $sortie){
            $this->id = $id;
            $this->id_chap = $id_chap;
            $this->id_ex = $id_ex;
            $this->entree = $entree;
            $this->sortie = $sortie;
        }

        /**
         * Fonction permettant de récupérer un jeu de test en BD selon l'id de son chapitre et l'id de son exercice.
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id_chap id du chapitre
         * @param Integer   $id_ex   id de l'exercice
         */
        public static function getJeuTestDB($pdo, $id_chap, $id_ex){
            $requete = $pdo->prepare("SELECT * FROM jeutest WHERE id_chap = :id_chap AND id_ex = :id_ex");
            $requete->execute(array( ':id_chap' => (int)$id_chap, ':id_ex' => (int)$id_ex));
            
            $row = $requete->fetchAll();
            
            if(!$row){
                throw new JeuTestException("Il n'existe aucun jeu de test pour cet exercice !", 1);
            } else {
                foreach($row as $jeu){
                    $array[] = new JeuTest($jeu['id'],$jeu['id_chap'],$jeu['id_ex'],$jeu['entree'],$jeu['sortie']);
                }
                return $array;
            }
        }

        /**
         * Fonction permettant de vérifier si un jeu de test portant cet id existe déjà en BD.
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id      id du jeu de test
         */
        public static function jeutestExisteByID($pdo, $id){
            $requete = $pdo->prepare("SELECT * FROM jeutest WHERE id = :id");
            $requete->execute( array( ':id' => (int)$id));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);

            if(!$row){
                return false;
            } else {
               return true;
            }
        }

        /**
         * Fonction permettant de créer un jeu de test en BD.
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id_chap id du chapitre
         * @param Integer   $id_ex   id de l'exercice
         * @param String    $entree  entree
         * @param String    $sortie  sortie
         */
        public static function createJeuTestDB($pdo, $id_chap, $id_ex, $entree, $sortie){
            try {
                $requete = $pdo->prepare("INSERT INTO jeutest (id_chap, id_ex, entree, sortie) VALUES (:id_chap, :id_ex, :entree, :sortie )");
                $requete->bindParam(':id_chap', $id_chap);
                $requete->bindParam(':id_ex', $id_ex);
                $requete->bindParam(':entree', $entree);
                $requete->bindParam(':sortie', $sortie);
                $requete->execute();
                return true;
            } catch(Exception $e){
                throw $e;
            }
        }

        /**
         * Fonction permettant de modifier un jeu de test en BD.
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id      id du jeu de test
         * @param Integer   $id_chap id du chapitre
         * @param Integer   $id_ex   id de l'exercice
         * @param String    $entree  entree
         * @param String    $sortie  sortie
         */
        public static function updateJeuTestDB($pdo, $id, $id_chap, $id_ex, $entree, $sortie){
            if( ! JeuTest::jeutestExisteByID($pdo, $id) ) {
                throw new JeuTestException("Ce jeu de test n'existe pas !", 1);
            } else {
                try {
                    $requete = $pdo->prepare("UPDATE jeutest SET id_chap = :id_chap, id_ex = :id_ex,entree = :entree, sortie = :sortie WHERE id = :id");
                    $requete->bindParam(':id', $id);
                    $requete->bindParam(':id_chap', $id_chap);
                    $requete->bindParam(':id_ex', $id_ex);
                    $requete->bindParam(':entree', $entree);
                    $requete->bindParam(':sortie', $sortie);
                    $requete->execute();
                    return true;
                } catch(Exception $e){
                    throw $e;
                }
            }
        }

        /**
         * Fonction permettant de supprimer un jeu de test en BD.
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id      id du jeu de test
         */
        public static function deleteJeuTestDB($pdo, $id){
            if( ! JeuTest::jeutestExisteByID($pdo, $id) ) {
                throw new JeuTestException("Ce jeu de test n'existe pas !", 1);
            } else {
                $requete = $pdo->prepare("DELETE FROM jeutest WHERE id = :id");
                $requete->bindParam(':id', $id);
                $requete->execute();
                return true;
            }
        }

        /**
         * Fonction permettant de supprimer tous les jeux de test d'un exercice en DB.
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id_chap id du chapitre
         * @param Integer   $id_ex   id de l'exercice
         */
        public static function deleteAllJeuTestFromExoDB($pdo, $id_chap, $id_ex){
            $requete = $pdo->prepare("DELETE FROM jeutest WHERE id_chap = :id_chap AND id_ex = :id_ex");
            $requete->bindParam(':id_chap', $id_chap);
            $requete->bindParam(':id_ex', $id_ex);
            $requete->execute();
            return true;
        }
    }

    /**
     * Classe JeuTestException
     * Classe définissant les exceptions de l'objet JeuTest.
     */
    class JeuTestException extends Exception{
        public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
        }
    
        public function __toString() {
            return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        }
    }
?>