<?php
    /**
     * Classe Exercice
     * Classe définissant l'objet Exercice et les fonctions associées pour traîter l'objet en base de données.
     */
    class Exercice {
        public function __construct($id_chap, $id, $titre, $consigne, $corrige, $dependances, $main, $visible){
            $this->id_chap = $id_chap;
            $this->id = $id;
            $this->titre = $titre;
            $this->consigne = $consigne;
            $this->corrige = $corrige;
            $this->dependances = $dependances;
            $this->main = $main;
            $this->visible = $visible;
        }
        
        public function __toString(){
            return "Exercice " . $this->id . " : " . $this->titre;
        }

        /**
         * Fonction permettant de récupérer un exercice en BD selon son id.
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id      id du chapitre
         */        
        public static function getExerciceDB($pdo, $id_chap ,$id){
            $requete = $pdo->prepare("SELECT * FROM exercice WHERE id_chap = :id_chap AND id = :id");
            $requete->execute( array( ':id_chap' => (int)$id_chap, ':id' => (int)$id ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                throw new ExerciceException("Cet exercice n'existe pas !", 1);
            } else {
                return new Exercice(
                    $row['id_chap'],$row['id'],$row['titre'],$row['consigne'],$row['corrige'],$row['dependances'],$row['main'],$row['visible']
                );
            }
        }

        /**
         * Fonction permettant de récupérer un chapitre en BD selon l'id de son chapitre et son titre.
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id_chap id du chapitre
         * @param String    $titre   titre de l'exercice
         */
        public static function getExerciceByTitre($pdo, $id_chap ,$titre){
            $requete = $pdo->prepare("SELECT * FROM exercice WHERE id_chap = :id_chap AND titre = :titre");
            $requete->execute( array( ':id_chap' => (int)$id_chap, ':titre' => $titre ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                throw new ExerciceException("Cet exercice n'existe pas !", 1);
            } else {
                return new Exercice(
                    $row['id_chap'],$row['id'],$row['titre'],$row['consigne'],$row['corrige'],$row['dependances'],$row['main'],$row['visible']
                );
            }
        }

        /**
         * Fonction permettant de vérifier si un exercice portant ce titre existe déjà en BD.
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id_chap id du chapitre
         * @param String    $titre   titre de l'exercice
         */
        public static function exerciceExisteByNom($pdo, $id_chap, $titre){
            $requete = $pdo->prepare("SELECT * FROM exercice WHERE id_chap = :id_chap AND titre = :titre");
            $requete->execute(array(':id_chap' => (int)$id_chap, ':titre' => $titre ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                return false;
            } else {
               return true;
            }
        }

        /**
         * Fonction permettant de vérifier si un exercice portant cet id existe déjà en BD.
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id_chap id du chapitre
         * @param Integer   $id      id de l'exercice
         */
        public static function exerciceExisteByID($pdo, $id_chap, $id){
            $requete = $pdo->prepare("SELECT * FROM exercice WHERE id_chap = :id_chap AND id = :id");
            $requete->execute(array( ':id_chap' => (int)$id_chap, ':id' => (int)$id ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                return false;
            } else {
               return true;
            }
        }

        /**
         * Fonction permettant de créer un exercice en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $id_chap        id du chapitre
         * @param String    $titre          titre de l'exercice
         * @param String    $consigne       consigne de l'exercice
         * @param String    $corrige        corrige de l'exercice
         * @param String    $dependances    dependances de l'exercice
         * @param String    $main           main de l'exercice
         * @param Integer   $visible        0 si invisible, 1 si visible
         */        
        public static function createExerciceDB($pdo, $id_chap, $titre, $consigne, $corrige, $dependances, $main, $visible){
            if( Exercice::exerciceExisteByNom($pdo, $id_chap, $titre) ) {
                throw new ExerciceException("Il y a déjà un exercice dans ce chapitre portant ce nom.", 2);
            } else {
                $requete = $pdo->prepare("INSERT INTO exercice (id_chap, titre, consigne, corrige, dependances, main, visible) VALUES(:id_chap, :titre, :consigne, :corrige, :dependances, :main, :visible)");
                $requete->bindParam(':id_chap', $id_chap);
                $requete->bindParam(':titre', $titre);
                $requete->bindParam(':consigne', $consigne);
                $requete->bindParam(':corrige', $corrige);
                $requete->bindParam(':dependances', $dependances);
                $requete->bindParam(':main', $main);
                $requete->bindParam(':visible', $visible);
                $requete->execute();
                return true;
            }
        }
        
        /**
         * Fonction permettant de modifier un exercice en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $id_chap        id du chapitre
         * @param Integer   $id             id de l'exercice
         * @param String    $titre          titre de l'exercice
         * @param String    $consigne       consigne de l'exercice
         * @param String    $corrige        corrige de l'exercice
         * @param String    $dependances    dependances de l'exercice
         * @param String    $main           main de l'exercice
         * @param Integer   $visible        0 si invisible, 1 si visible
         */        
        public static function updateExerciceDB($pdo, $id_chap, $id, $new_chap, $titre, $consigne, $corrige, $dependances, $main, $visible){
            if( ! Exercice::exerciceExisteByID($pdo, $id_chap, $id) ) {
                throw new ExerciceException("Cet exercice n'existe pas !", 1);
            } else {
                $requete = $pdo->prepare("UPDATE exercice SET id_chap = :new_chap, titre = :titre, consigne = :consigne, corrige = :corrige, dependances = :dependances, main = :main, visible = :visible WHERE id_chap = :id_chap AND id = :id");
                $requete->bindParam(':new_chap', $new_chap);
                $requete->bindParam(':titre', $titre);
                $requete->bindParam(':consigne', $consigne);
                $requete->bindParam(':corrige', $corrige);
                $requete->bindParam(':dependances', $dependances);
                $requete->bindParam(':main', $main);
                $requete->bindParam(':visible', $visible);
                $requete->bindParam(':id_chap', $id_chap);
                $requete->bindParam(':id', $id);
                $requete->execute();
                return true;

            }
        }

        /**
         * Fonction permettant de supprimer un exercice en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $id_chap        id du chapitre
         * @param Integer   $id             id de l'exercice
         */
        public static function deleteExerciceDB($pdo, $id_chap, $id){
            if( ! Exercice::exerciceExisteByID($pdo, $id_chap, $id) ) {
                throw new ExerciceException("Cet exercice n'existe pas !", 1);
            } else {
                $requete = $pdo->prepare("DELETE FROM exercice WHERE id_chap = :id_chap AND id = :id");
                $requete->bindParam(':id_chap', $id_chap);
                $requete->bindParam(':id', $id);
                $requete->execute();
                return true;
            }
        }
    }

    /**
     * Classe ExerciceException
     * Classe définissant les exceptions de l'objet Exercice.
     */
    class ExerciceException extends Exception{
        public function __construct($message, $code = 0, Exception $previous = null) {
          parent::__construct($message, $code, $previous);
        }
      
        public function __toString() {
          return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        }
    }
?>