<?php

    /**
     * Classe Promo
     * Classe définissant l'objet Promo et les fonctions associées pour traîter l'objet en base de données.
     */
    class Promo {
        
        public function __construct($id, $annee){
            $this->id = $id;
            $this->annee = $annee;
        }
        
        public function __toString(){
            return "Promo " . $this->id . ", Annee : " . $this->annee;
        }

        /**
         * Fonction permettant de récupérer une promo en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $id             id de la promo
         */        
        public static function getPromoDB($pdo,$id){
            $requete = $pdo->prepare("SELECT * FROM promo WHERE id = :id");
            $requete->execute(array( ':id' => (int)$id ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                throw new PromoException("Cette promotion n'existe pas !", 1);
            } else {
                return new Promo(
                    $row['id'],$row['annee']
                );
            }
        }

        /**
         * Fonction permettant de récupérer toutes les promos en BD
         * @param PDO       $pdo            php data objects de la BD
         */
        public static function getAllPromoDB($pdo){
            $requete = $pdo->prepare("SELECT * FROM promo");
            $requete->execute(array());
            $row = $requete->fetchAll();

            if(!$row){
                throw new PromoException("Aucune promotion.", 4);
            } else {
                foreach($row as $prom){
                    $array[] = new Promo($prom['id'], $prom['annee']);
                }
                return $array;
            }
        }

        /**
         * Fonction permettant de récupérer une promo  en BD selon son en année
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $annee          annee de la promo
         */
        public static function getPromoByAnnee($pdo,$annee){
            $requete = $pdo->prepare("SELECT * FROM promo WHERE annee = :annee");
            $requete->execute(array( ':annee' => (int)$annee ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                throw new PromoException("Cette promotion n'existe pas !", 1);
            } else {
                return new Promo(
                    $row['id'],$row['annee']
                );
            }
        }

        /**
         * Fonction permettant de vérifier si une promo ayant cette annee existe déjà en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $annee          annee de la promo
         */
        public static function promoExisteByAnnee($pdo,$annee){
            $requete = $pdo->prepare("SELECT * FROM promo WHERE annee = :annee");
            $requete->execute(array( ':annee' => $annee ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                return false;
            } else {
               return true;
            }
        }

        /**
         * Fonction permettant de vérifier si une promo ayant cet id existe déjà en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $id             id de la promo
         */ 
        public static function promoExisteByID($pdo,$id){
            $requete = $pdo->prepare("SELECT * FROM promo WHERE id = :id");
            $requete->execute(array( ':id' => (int)$id ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                return false;
            } else {
               return true;
            }
        }

        /**
         * Fonction permettant de créer une promo en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $annee          annee de la promo
         */
        public static function createPromoDB($pdo, $annee){
            if( Promo::promoExisteByAnnee($pdo,$annee) ) {
                throw new PromoException("Il existe déjà un promotion associée à cette année.", 2);
            } else {
                $requete = $pdo->prepare("INSERT INTO promo (annee) VALUES(:annee)");
                $requete->bindParam(':annee', $annee);
                $requete->execute();
                return true;

                
            }
        }

        /**
         * Fonction permettant de supprimer une promo en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $id             id de la promo
         */
        public static function deletePromoDB($pdo, $id){
            if( ! Promo::promoExisteByID($pdo,$id) ) {
                throw new PromoException("Cette promotion n'existe pas !", 1);
            } else {
                $requete = $pdo->prepare("DELETE FROM promo WHERE id = :id");
                $requete->bindParam(':id', $id);
                $requete->execute();
                return true;
            }
        }

        /**
         * Fonction permettant de récupérer les élèves d'une promotion en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $id             id de la promo
         */
        public static function eleveFromPromo($pdo, $id){
            $requete = $pdo->prepare("SELECT * FROM utilisateur WHERE promo = :id");
            $requete->execute(array( ':id' => (int)$id ));
            
            $row = $requete->fetchAll();
            
            if(!$row){
                throw new PromoException("Il n'y a pas d'élèves dans cette promotion.", 3);
            } else {
                foreach($row as $eleve){
                    $array[] = new Utilisateur($eleve['id'],$eleve['nom'],$eleve['mdp'],$eleve['type'],$eleve['promo']);
                }
                return $array;
            }
        }
    }

    /**
     * Classe PromoException
     * Classe définissant les exceptions de l'objet Promo.
     */
    class PromoException extends Exception{
        public function __construct($message, $code = 0, Exception $previous = null) {
          parent::__construct($message, $code, $previous);
        }
      
        public function __toString() {
          return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        }
    }

?>