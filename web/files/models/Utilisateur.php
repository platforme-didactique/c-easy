<?php
    /**
     * Fonction permettant de générer un mot de passe aléatoire.
     * @param Integer   $taille         longueur du mot de passe
     */
    function randomPassword($taille) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789!#-*/+?%$";
        $mdp = array();
        $longueur = strlen($alphabet) - 1;
        for ($i = 0; $i < $taille; $i++) {
            $n = rand(0, $longueur);
            $mdp[] = $alphabet[$n];
        }
        return implode($mdp);
    }

    /**
     * Classe Utilisateur
     * Classe définissant l'objet Utilisateur et les fonctions associées pour traîter l'objet en base de données.
     */
    class Utilisateur {
        public function __construct($id, $nom, $mdp, $type, $promo){
            $this->id = $id;
            $this->nom = $nom;
            $this->mdp = $mdp;
            $this->type = $type; 
            $this->promo = $promo;
        }

        /**
         * Fonction permettant de récupérer un utilisateur en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $id             id de l'utilisateur
         */          
        public static function getUtilisateurDB($pdo,$id){
            $requete = $pdo->prepare("SELECT * FROM utilisateur WHERE id = :id");
            $requete->execute(array( ':id' => (int)$id ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                throw new UtilisateurException("Utilisateur inexistant", 1);
            } else {
                return new Utilisateur(
                    $row['id'],$row['nom'],$row['mdp'],$row['type'],$row['promo']
                );
            }
        }

        /**
         * Fonction permettant de récupérer tous les administrateurs de la BD
         * @param PDO       $pdo            php data objects de la BD
         */         
        public static function getAllAdminDB($pdo){
            $requete = $pdo->prepare("SELECT * FROM utilisateur where type != 1 OR type is NULL");
            $requete->execute(array());
            $row = $requete->fetchAll();

            if(!$row){
                throw new UtilisateurException("Il n'y a pas de compte administrateur.", 4);
            } else {
                $array=array();
                foreach($row as $user){
                    $array []= new Utilisateur($user['id'],$user['nom'],$user['mdp'],$user['type'],$user['promo']);
                }
                return ($array);
            }
        }

        /**
         * Fonction permettant de récupérer tous les utilisateurs d'une promo de la BD
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $promo          id de la promo
         */   
        public static function getUtilisateursByPromo($pdo,$promo){
            $requete = $pdo->prepare("SELECT * FROM utilisateur WHERE promo = :promo");
            $requete->execute(array( ':promo' => (int)$promo ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                throw new UtilisateurException("Cet utilisateur n'existe pas !", 1);
            } else {
                $array=array();
                foreach($row as $user){
                    $array []= new Utilisateur($user['id'],$user['nom'],$user['mdp'],$user['type'],$user['promo']);
                }
                return ($array);
            }
        }
        /**
         * 
         * Fonction permettant de vérifier si un utilisateur portant ce nom existe déjà en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param String    $nom            nom de l'utilisateur
         * @param Integer   $promo          id de la promo
         */   
        public static function utilisateurExistByNom($pdo,$nom, $promo){
            $requete = $pdo->prepare("SELECT * FROM utilisateur WHERE nom = :nom AND promo = :promo");
            $requete->execute(array( ':nom' => $nom, ':promo' => (int)$promo ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                return false;
            } else {
                return true;
            }
        }

        /**
         * Fonction de connexion vérifiant si le mot de passe entré par l'utilisateur correspond à celui renseigné en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param String    $nom            nom de l'utilisateur
         * @param String    $mdp            mdp de l'utilisateur
         */
        public static function verificationConnection($pdo, $nom, $mdp){
            $requete = $pdo->prepare("SELECT * FROM utilisateur WHERE nom = :nom and mdp = SHA1(:mdp)");
            $requete->execute(array( ':nom' => $nom, ':mdp' => $mdp));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                throw new UtilisateurException("Identification échouée", 2);
            } else {
                return new Utilisateur(
                    $row['id'],$row['nom'],$row['mdp'],$row['type'],$row['promo']
                );
            }
        }

        /**
         * Fonction permettant de créer un utilisateur élève en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param String    $nom            nom de l'utilisateur
         * @param Integer   $promo          id de la promo
         */
        public static function createUserDB($pdo, $nom, $promo){
            if( Utilisateur::utilisateurExistByNom($pdo,$nom, $promo) ) {
                throw new UtilisateurException("Cet utilisateur existe déjà !", 3);
            } else {
                $mdp = randomPassword(6);
                $requete = $pdo->prepare("INSERT INTO utilisateur (nom,mdp,promo,type) VALUES(:nom, SHA1(:mdp), :promo, 1)");
                $requete->bindParam(':nom', $nom);
                $requete->bindParam(':mdp', $mdp);
                $requete->bindParam(':promo', $promo);
                $requete->execute();

                return $mdp;
            }
        }

        /**
         * Fonction permettant de créer un administrateur en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param String    $nom            nom de l'utilisateur
         * @param String    $mdp            mdp de l'utilisateur
         */
        public static function createUserAdminDB($pdo, $nom, $mdp){
            if( Utilisateur::utilisateurExistByNom($pdo,$nom, NULL) ) {
                throw new UtilisateurException("Cet utilisateur existe déjà !", 3);
            } else {
                $requete = $pdo->prepare("INSERT INTO utilisateur (nom,mdp,type) VALUES(:nom, SHA1(:mdp), 0)");
                $requete->bindParam(':nom', $nom);
                $requete->bindParam(':mdp', $mdp);
                $requete->execute();

                return $mdp;
            }
        }

        /**
         * Fonction permettant de récupérer les exercices validés par l'utilisateur selon un chapitre en BD
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $id_eleve       id de l'utilisateur
         * @param Integer   $id_chap        id du chapitre
         */
        public static function getExosValideByChapitre($pdo, $id_eleve, $id_chap){
            $requete = $pdo->prepare("SELECT E.* FROM exercice E INNER JOIN realise R ON E.id_chap = R.id_chap AND E.id = R.id_ex  WHERE R.id_eleve = :id_eleve AND R.id_chap = :id_chap AND R.valide = 1");
            $requete->execute(array( ':id_eleve' => (int)$id_eleve, ':id_chap' => (int)$id_chap ));
            
            $row = $requete->fetchAll();

            $array=array();
            foreach($row as $exo){
                $array[] = new Exercice($exo['id_chap'],$exo['id'],$exo['titre'],$exo['consigne'],$exo['corrige'],$exo['dependances'],$exo['main'],$exo['visible']);
            }
            return ($array);
        }
    }
    
    /**
     * Classe RealiseException
     * Classe définissant les exceptions de l'objet Realise.
     */
    class UtilisateurException extends Exception{
        public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
        }

        public function __toString() {
            return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        }

    }
?>