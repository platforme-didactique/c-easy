<?php
    /**
     * Classe Chapitre
     * Classe définissant l'objet Chapitre et les fonctions associées pour traîter l'objet en base de données.
     */
    class Chapitre {

        public function __construct($id, $nom, $indice, $visible){
            $this->id = $id;
            $this->nom = $nom;
            $this->indice = $indice;
            $this->visible = $visible;
        }
        
        public function __toString(){
            return "Chapitre " . $this->id . " : " . $this->nom;
        }
        
        /**
         * Fonction permettant de récupérer un chapitre en BD selon son id.
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id      id du chapitre
         */
        public static function getChapitreDB($pdo,$id){
            $requete = $pdo->prepare("SELECT * FROM chapitre WHERE id = :id");
            $requete->execute(array( ':id' => (int)$id ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                throw new ChapitreException("Chapitre inexistant", 1);
            } else {
                return new Chapitre(
                    $row['id'],$row['nom'],$row['indice'],$row['visible']
                );
            }
        }

        /**
         * Fonction permettant de récupérer tous les chapitres de la BD.
         * @param PDO       $pdo     php data objects de la BD
         */        
        public static function getAllChapitreDB($pdo){
            $requete = $pdo->prepare("SELECT * FROM chapitre");
            $requete->execute(array());
            $row = $requete->fetchAll();

            if(!$row){
                throw new ChapitreException("Il n'y a pas de chapitre.", 4);
            } else {
                foreach($row as $chap){
                    $array[] = new Chapitre($chap['id'], $chap['nom'], $chap['indice'], $chap['visible']);
                }
                return $array;
            }
        }

        /**
         * Fonction permettant de vérifier si un chapitre porte déjà ce nom en BD.
         * @param PDO       $pdo     php data objects de la BD
         * @param String    $nom     nom du chapitre
         */        
        public static function chapitreExisteByNom($pdo,$nom){
            $requete = $pdo->prepare("SELECT * FROM chapitre WHERE nom = :nom");
            $requete->execute(array( ':nom' => $nom ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                return false;
            } else {
               return true;
            }
        }

        /**
         * Fonction permettant de récupérer un chapitre en BD selon son nom.
         * @param PDO       $pdo     php data objects de la BD
         * @param String    $nom     nom du chapitre
         */        
        public static function getChapitreByNom($pdo, $nom){
            $requete = $pdo->prepare("SELECT * FROM chapitre WHERE nom = :nom");
            $requete->execute(array( ':nom' => $nom ));
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                throw new ChapitreException("Chapitre inexistant", 1);
            } else {
                return new Chapitre(
                    $row['id'],$row['nom'],$row['indice'],$row['visible']
                );
            }
        }

        /**
         * Fonction permettant de vérifier si le chapitre existe en BD.
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id      id du chapitre
         */        
        public static function chapitreExisteByID($pdo,$id){
            $requete = $pdo->prepare("SELECT * FROM chapitre WHERE id = :id");
            $requete->execute(array( ':id' => (int)$id ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                return false;
            } else {
               return true;
            }
        }

        /**
         * Fonction permettant de créer un chapitre en BD
         * @param PDO       $pdo     php data objects de la BD
         * @param String   $nom      nom du chapitre
         * @param String    $indice  indice relatif au chapitre.
         * @param Integer   $visible 0 si invisible, 1 si visible
         */        
        public static function createChapitreDB($pdo, $nom, $indice, $visible){
            if( Chapitre::chapitreExisteByNom($pdo,$nom) ) {
                throw new ChapitreException("Il existe déjà un chapitre portant ce nom.", 2);
            } else {
                $requete = $pdo->prepare("INSERT INTO chapitre (nom,indice,visible) VALUES(:nom, :indice, :visible)");
                $requete->bindParam(':nom', $nom);
                $requete->bindParam(':indice', $indice);
                $requete->bindParam(':visible', $visible);
                $requete->execute();
                return true;
            }
        }
        
        /**
         * Fonction permettant de modifier un chapitre en BD
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id      id du chapitre
         * @param String    $nom     nom du chapitre
         * @param String    $indice  indice relatif au chapitre.
         * @param Integer   $visible 0 si invisible, 1 si visible
         */   
        public static function updateChapitreDB($pdo, $id, $nom, $indice, $visible){
            if( ! Chapitre::chapitreExisteByID($pdo,$id) ) {
                throw new ChapitreException("Ce chapitre n'existe pas !", 1);
            } else {
                $requete = $pdo->prepare("UPDATE chapitre SET nom = :nom, indice = :indice, visible = :visible WHERE id = :id");
                $requete->bindParam(':nom', $nom);
                $requete->bindParam(':indice', $indice);
                $requete->bindParam(':visible', $visible);
                $requete->bindParam(':id', $id);
                $requete->execute();
                return true;
            }
        }

        /**
         * Fonction permettant de supprimer un chapitre en BD selon son id.
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id      id du chapitre
         */
        public static function deleteChapitreDB($pdo, $id){
            if( ! Chapitre::chapitreExisteByID($pdo,$id) ) {
                throw new ChapitreException("Ce chapitre n'existe pas !", 1);
            } else {
                $requete = $pdo->prepare("DELETE FROM chapitre WHERE id = :id");
                $requete->bindParam(':id', $id);
                $requete->execute();
                return true;
            }
        }

        /**
         * Fonction permettant de récupérer les exercices d'un chapitre en BD selon son id.
         * @param PDO       $pdo     php data objects de la BD
         * @param Integer   $id      id du chapitre
         */        
        public static function exerciceFromChapitre($pdo, $id){
            $requete = $pdo->prepare("SELECT * FROM exercice WHERE id_chap = :id");
            $requete->execute(array( ':id' => (int)$id ));
            
            $row = $requete->fetchAll();
            
            if(!$row){
                throw new ChapitreException("Il n'y pas d'exercice dans ce chapitre.", 3);
            } else {
                foreach($row as $exo){
                    $array[] = new Exercice($exo['id_chap'],$exo['id'],$exo['titre'],$exo['consigne'],$exo['corrige'],$exo['dependances'],$exo['main'],$exo['visible']);
                }
                return $array;
            }
        }
    }

    /**
     * Classe ChapitreException
     * Classe définissant les exceptions de l'objet Chapitre.
     */
    class ChapitreException extends Exception{
        public function __construct($message, $code = 0, Exception $previous = null) {
          parent::__construct($message, $code, $previous);
        }
      
        public function __toString() {
          return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        }
    }

?>