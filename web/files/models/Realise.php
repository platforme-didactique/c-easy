<?php
    /**
     * Classe Realise
     * Classe définissant l'objet Realise et les fonctions associées pour traîter l'objet en base de données.
     */
    class Realise {
        public function __construct($id_chap, $id_ex, $id_eleve, $temps_debut, $temps_valide, $nb_essai, $valide, $code){
            $this->id_chap = $id_chap;
            $this->id_ex = $id_ex;
            $this->id_eleve = $id_eleve;
            $this->temps_debut = $temps_debut;
            $this->temps_valide = $temps_valide;
            $this->nb_essai = $nb_essai;
            $this->valide = $valide;
            $this->code = $code;
        }
        
        public function __toString(){
            return "Realise " . $this->id_ex . " : " . $this->id_eleve;
        }

        /**
         * Fonction permettant de récupérer realise en BD.
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $id_ex          id de l'exercice
         * @param Integer   $id_chap        id du chapitre
         * @param Integer   $id_eleve       id de l'eleve
         */       
        public static function getRealiseDB($pdo, $id_ex, $id_chap, $id_eleve){
            $requete = $pdo->prepare("SELECT * FROM realise WHERE id_chap = :id_chap AND id_ex = :id_ex AND id_eleve = :id_eleve");
            $requete->execute( array( ':id_chap' => (int)$id_chap, ':id_ex' => (int)$id_ex, ':id_eleve' => (int)$id_eleve ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);
            
            if(!$row){
                throw new RealiseException("Realise inexistant", 1);
            } else {
                return new Realise(
                    $row['id_chap'],$row['id_ex'],$row['id_eleve'],$row['temps_debut'],$row['temps_valide'],$row['nb_essai'],$row['valide'],$row['code']
                );
            }
        }

        /**
         * Fonction permettant de vérifier si un élève réalise déjà cet exercice.
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $id_ex          id de l'exercice
         * @param Integer   $id_chap        id du chapitre
         * @param Integer   $id_eleve       id de l'eleve
         */ 
        public static function realiseExisteByID($pdo, $id_ex, $id_chap, $id_eleve){
            $requete = $pdo->prepare("SELECT * FROM realise WHERE id_chap = :id_chap AND id_ex = :id_ex AND id_eleve = :id_eleve");
            $requete->execute( array( ':id_chap' => (int)$id_chap, ':id_ex' => (int)$id_ex, ':id_eleve' => (int)$id_eleve ));
    
            $row = $requete->fetch(PDO::FETCH_ASSOC);

            if(!$row){
                return false;
            } else {
               return true;
            }
        }

        /**
         * Fonction permettant de créer realise en BD lorsqu'un élève démarre un nouvel exercice.
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $id_ex          id de l'exercice
         * @param Integer   $id_chap        id du chapitre
         * @param Integer   $id_eleve       id de l'eleve
         */ 
        public static function createRealiseDB($pdo, $id_ex, $id_chap, $id_eleve){
            if ( Realise::realiseExisteByID($pdo, $id_ex, $id_chap, $id_eleve)) {
                throw new RealiseException("Realise existant", 2);
            } else {
                try {
                    $requete = $pdo->prepare("INSERT INTO realise (id_ex, id_chap, id_eleve, temps_debut) VALUES (:id_ex, :id_chap, :id_eleve, now() )");
                    $requete->bindParam(':id_ex', $id_ex);
                    $requete->bindParam(':id_chap', $id_chap);
                    $requete->bindParam(':id_eleve', $id_eleve);
                    $requete->execute();
                    return true;
                } catch(Exception $e){
                    echo $e;
                }
  
            }
        }

        /**
         * Fonction permettant de modifier realise en BD lorsqu'un élève échoue un test
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $id_ex          id de l'exercice
         * @param Integer   $id_chap        id du chapitre
         * @param Integer   $id_eleve       id de l'eleve
         */ 
        public static function updateRealiseDB($pdo, $id_ex, $id_chap, $id_eleve){
            if( ! Realise::realiseExisteByID($pdo, $id_ex, $id_chap, $id_eleve) ) {
                throw new RealiseException("Realise inexistant", 1);
            } else {
                $requete = $pdo->prepare("UPDATE realise SET nb_essai = nb_essai + 1, temps_valide = now() WHERE id_ex = :id_ex AND id_chap = :id_chap AND id_eleve = :id_eleve");
                $requete->bindParam(':id_ex', $id_ex);
                $requete->bindParam(':id_chap', $id_chap);
                $requete->bindParam(':id_eleve', $id_eleve);
                $requete->execute();
                return true;
            }
        }

        /**
         * Fonction permettant de modifier realise en BD lorsqu'un élève valide les tests
         * @param PDO       $pdo            php data objects de la BD
         * @param Integer   $id_ex          id de l'exercice
         * @param Integer   $id_chap        id du chapitre
         * @param Integer   $id_eleve       id de l'eleve
         * @param String    $code           code avec lequel l'élève a validé l'exercice
         */ 
        public static function valideRealiseDB($pdo, $id_ex, $id_chap, $id_eleve, $code){
            if( ! Realise::realiseExisteByID($pdo, $id_ex, $id_chap, $id_eleve) ) {
                throw new RealiseException("Realise inexistant", 1);
            } else {
                $requete = $pdo->prepare("UPDATE realise SET nb_essai = nb_essai + 1, temps_valide = now(), valide = 1, code = :code WHERE id_ex = :id_ex AND id_chap = :id_chap AND id_eleve = :id_eleve");
                $requete->bindParam(':id_ex', $id_ex);
                $requete->bindParam(':id_chap', $id_chap);
                $requete->bindParam(':id_eleve', $id_eleve);
                $requete->bindParam(':code', $code);
                $requete->execute();
                return true;
            }
        }           
    }

    /**
     * Classe RealiseException
     * Classe définissant les exceptions de l'objet Realise.
     */
    class RealiseException extends Exception{
        public function __construct($message, $code = 0, Exception $previous = null) {
          parent::__construct($message, $code, $previous);
        }
      
        public function __toString() {
          return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        }
    }

?>