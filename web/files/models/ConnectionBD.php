<?php
    /**
     * Classe ConnectionDB
     * Singleton de la connexion à la base de données.
     */
    class ConnectionDB{
        private static $instance = null;
        private $conn;
        
        private $host = '172.58.0.3';
        private $user = 'web';
        private $pass = 'mdp';
        private $name = 'bd';
         
        private function __construct(){
            try {
                $this->conn = new PDO("mysql:host={$this->host};dbname={$this->name}", $this->user,$this->pass);
            } catch ( PDOException $e ) {
                echo 'ERROR!';
                print_r( $e );
            }
        }
        
        public static function getInstance(){
          if(!self::$instance){
            self::$instance = new ConnectionDB();
          }
         
          return self::$instance;
        }
        
        public function getConnection(){
          return $this->conn;
        }
      };
?>