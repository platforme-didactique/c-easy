$(document).ready(function () {
    var editor = ace.edit("editor");
    editor.setOptions({
        selectionStyle: "text",
        highlightSelectedWord: true,
        fontSize: 15,
        mode: "ace/mode/c_cpp",
        theme: "ace/theme/monokai"
    });

    editor.commands.addCommand({
        name: "myCommand",
        bindKey: {
            win: 'Ctrl-Enter',
            mac: 'Command-Enter'
        },
        exec: function (editor) {
            $("#compileButton").click();
        }
    });

    compile = function (uid, chapitre, exercice) {
        var code = editor.getSession().getValue();

        if (code.includes("sandstorm")) {
            window.open(
                'https://www.youtube.com/tv#/watch/video/control?v=y6120QOlsfU&resume',
                '_blank'
            );
        }

        if (code.includes("crouton")) {
            window.open(
                'http://www.crouton.net',
                '_blank'
            );
        }

        if (code.includes("rainbow")) {
           $('body').addClass('AEC');
        }

        switch (code) {
            case '':
                alert("Le code va pas s'écrire tout seul, ça sert à rien d'essayer");
                break;
            default:
                $.ajax({
                    type: "POST",
                    url: 'compiler/compilation.php',
                    data: {
                        action: 'compilation',
                        uid: uid,
                        chapitre: chapitre,
                        exercice: exercice,
                        code: code
                    },
                    beforeSend: function () {
                        $('#compileButton').attr("disabled", true);
                        $('#compileButton').html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Compilation en cours...");
                    },
                    success: function (data) {
                        data = JSON.parse(data);
                        if (data.success == 'true') {
                            if (data.resultat && Array.isArray(data.resultat)) {
                                var test = "";
                                data.resultat.forEach(function (element) {
                                    if (element.valide) {
                                        $("#valideIcone").html("Validé <i class='fas fa-check'></i>");
                                        $("#valideIcone").removeClass("text-danger").addClass("text-success");
                                        test += "Vous avez réussi les tests !";
                                    } else {
                                        test += "Essayez encore !";
                                    }
                                    test+="</p><p>";
                                    element.test.forEach(function (elem) {
                                        if (elem.output.includes("infinie")) {
                                            test = "Nous sommes en présence d'une boucle infinie, corrigez votre programme avant de recompiler.";
                                        } else {
                                            test += "Entrée : " + elem.entree + " "; 
                                            test += "Sortie : " + elem.output + " ";
                                            test += "Sortie attendue : " + elem.sortie + " ";
                                            test += (elem.passe == true ? "<i class='fas fa-check text-success'></i>" : "<i class='fas fa-times text-danger'></i>" )+ "<br>";
                                        }
                                    });
                                });
                                test+="</p>";
                                $("#collapseText").html("<p> Résultat des tests : " + test);
                                $('#compileButton').removeAttr("disabled");
                                $('#compileButton').html("Compiler");
                            }
                        } else {
                            if (Array.isArray(data.message)) {
                                var error = "";
                                data.message.forEach(function (element) {
                                    error += "" + element + "<br>";
                                });
                            } else {
                                var error = data.message;
                            }
                            $("#collapseText").html(error);
                            $('#compileButton').removeAttr("disabled");
                            $('#compileButton').html("Compiler");
                        }
                    }
                });
        }
    };
});