#!/bin/bash

($( echo $(./$1 $2) > res$1 ) & )
sleep 0.1
while pgrep -x "$1" > /dev/null
do
	cpulimit -e "$1" -l 3;
	sleep 0.1;
done
sleep 0.1;
cat res$1;
sleep 0.1;
rm res$1;