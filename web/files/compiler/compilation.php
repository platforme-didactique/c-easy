<?php
    require "../models/ConnectionBD.php";
	require "../models/Exercice.php";
    require "../models/Utilisateur.php";
    require "../models/JeuTest.php";
    require "../models/Realise.php";

    
	$singleton = ConnectionDB::getInstance();
    $conn = $singleton->getConnection();

    /**
     * Fonction permettant de créer un exécutable pour un code donné.
     * @param JeuTest   $jeutest     jeux de test associés à l'exo
     * @param Integer   $idexo       id de l'exercice
     * @param Integer   $ideleve     ide de l'eleve
     * @param String    $filename    nom du fichier à créer
     */
    function test($conn, $jeutest, $ideleve, $file){
        if ($jeutest) {
            // Pour chaque jeu de test, on exécute et vérifie le output
            foreach($jeutest as $row) {
                $idex = $row->id_ex;
                $idchap = $row->id_chap;
                $output = array();
                $sortie = explode(",",$row->sortie);
                exec("timeout 5 ./exec.sh '" . $file . "' '" . $row->entree . "' 2>&1 || echo 'infinie'", $output);
                //exec("timeout 10 ./" . $file . " " . $row->entree . " 2>&1 || echo 'infinie'", $output);
                // Si la sortie est égale à l'output, on renvoie true
                if ($sortie == $output){
                    $test[] =  array("entree" => $row->entree, "sortie" => $row->sortie, "output" => $output, "passe" => true);
                // Sinon false
                } else {
                    $test[] =  array("entree" => $row->entree, "sortie" => $row->sortie, "output" => $output, "passe" => false);
                }
            }

            $valide = true;
            // Pour chaque test, on vérifie si l'utilisateur l'a passé. 
            foreach($test as $result){
                $valide *= $result['passe'];
            }

            // Si l'utilisateur a déjà complété l'exo, il peut le refaire sans incrémenter son nombre de tentative
            if(Realise::getRealiseDB($conn, $idex, $idchap, $ideleve)->valide == "0"){
                // Si valide est true, alors on valide l'exercice pour l'utilisateur
                if($valide){
                    try {
                        Realise::valideRealiseDB($conn, $idex, $idchap, $ideleve, $_POST['code']);
                    } catch (Exception $e) {
                    throw $e;
                    }
                // Sinon, on lui compte une tentative supplémentaire
                } else {
                    try {
                        Realise::updateRealiseDB($conn, $idex, $idchap, $ideleve);
                    } catch (Exception $e) {
                        throw $e;
                    }
                }
            }

            $array[] = array("valide" => $valide, "test" => $test);
        }

        // On supprime l'exécutable
        exec("rm " . $file);
        // On renvoie une array avec l'entrée, la sortie attendue, la sortie obtenue et si le test a été réussi.
        return $array;
    }
    
    /**
     * Fonction permettant de créer un exécutable pour un code donné.
     * @param String    $code        code de l'utilisateur
     * @param Exercice  $exercice    exercice en cours
     * @param String    $filename    nom du fichier à créer
     */
    function compile($code ,$exercice, $filename){
        // On parse le contenu du fichier en récupérant les dépédances et le main de l'exercice.
        if(strpos($code, '#include') !== false){
            return array("success" => "false", "message" => "Vous n'avez pas le droit de déclarer de nouvelles dépendances !"); 
        } else {
            $dependencies = $exercice->dependances;
            $code = $dependencies . "\n" . $code . "\n";
            $main = $exercice->main;
            $code .= $main;
            // Création d'un fichier contenant le code
            file_put_contents($filename . ".c", $code);
            // On compile le fichier en redirigeant les erreurs dans le stdout
            exec("gcc -Wall -Werror " . $filename . ".c -o" . $filename ." 2>&1", $output);
            // Après compilation, on supprime le fichier .c
            exec("rm " . $filename . ".c");
            // Si c'est bien compilé, pas de $output sinon c'est une Array d'erreurs.
            if(count($output) > 0){
                return array("success" => "false", "message" => $output);
            } else {
                return array("success" => "true", "message" => "Fichier compilé");
            }
        }
    }

    // On vérifie si le body contient bien le type d'action
    if(isset($_POST['action']) && $_POST['action'] == 'compilation'){
        // On vérifie l'existence du champs code, exercice et uid.
        if(isset($_POST['code']) && isset($_POST['chapitre']) && isset($_POST['exercice']) && isset($_POST['uid'])){
            try {
                // On extrait l'utilisateur de la base de données
                $utilisateur = Utilisateur::getUtilisateurDB($conn, $_POST['uid']);
                // On extrait l'exercice de la base de données
                $exercice = Exercice::getExerciceDB($conn, $_POST['chapitre'], $_POST['exercice']);
                // Fichier sous la forme de "id_exo-id_user" 
                $filename = $exercice->id . "-" .  $utilisateur->id;
                // On lance la compilation
                $result = compile($_POST['code'],$exercice, $filename);
                // Si la compilation a fonctionné, on peut tester
                if($result['success'] == "true"){
                    try {
                        $jeutest = JeuTest::getJeuTestDB($conn, $exercice->id_chap, $exercice->id);
                        $test = test($conn,$jeutest, $_POST['uid'], $filename);
                        echo json_encode(array_merge($result, array("resultat" => $test)));
                    } catch(Exception $e){
                        echo json_encode(array("success" => "false", "message" => $e->getMessage()));
                    }

                } else {
                    echo json_encode($result);
                }
            } catch (Exception $e){
                // Si l'utilisateur ou l'exercice est incorrect, on retourne une erreur qui correspond à l'exception
                echo json_encode(array("success" => "false", "message" => $e->getMessage()));
            }
        } else {
            echo json_encode(array("success" => "false", "message" => "Un ou plusieurs champs sont invalides"));
        }
    } else  {
        echo "Failed";
    }
?>