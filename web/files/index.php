<?php
    session_start();

    $D=DIRECTORY_SEPARATOR;
    $ROOT_FOLDER=__DIR__.$D."lib".$D."File.php";
    require_once ($ROOT_FOLDER);
    require_once "models/ConnectionBD.php";
    $singleton = ConnectionDB::getInstance();
    $conn = $singleton->getConnection();

    foreach (glob("models/*.php") as $filename){
        require_once $filename;
    }
?>

<!DOCTYPE html>
<html>

<head>
	<!-- META DATA -->
	<title>C-Easy</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta name="description" content="Plateforme didactique pour le langage C." />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />

	<link rel="icon" type="image/png" href="lib/logo.png">

	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="lib/logo.png">
	<meta name="theme-color" content="##00adef">


	<meta property="og:site_name " content="C-Easy" />
	<meta property="og:title" content="C-Easy" />
	<meta property="og:description" content="Plateforme didactique pour le langage C." />

	<!-- CSS -->
	<link rel="stylesheet" href="style/bootstrap.min.css">
	<link rel="stylesheet" href="style/editor.css">
	<link rel="stylesheet" href="style/style.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
		integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.13.0/themes/prism.css">
	<link rel="stylesheet" href="style/codemirror.min.css">
	<link href="lib/trumbowyg/trumbowyg.min.css" rel="stylesheet" type="text/css">

	<!-- JS+ -->
	<script src="view/js/jspdf.min.js"></script>
	<script src="view/js/jquery-3.4.1.min.js"></script>
	<script src="view/js/popper.min.js"></script>
	<script type="text/javascript" src="view/js/codemirror.min.js"></script>
	<script type="text/javascript" src="view/js/xml.min.js"></script>

	<script src="//cdnjs.cloudflare.com/ajax/libs/prism/1.13.0/prism.min.js"></script>
	<script type="text/javascript" src="lib/trumbowyg/trumbowyg.min.js"></script>
	<script type="text/javascript" src="lib/trumbowyg/fr.min.js"></script>
	<script type="text/javascript" src="lib/trumbowyg/trumbowyg.pasteembed.min.js"></script>
	<script type="text/javascript" src="lib/trumbowyg/trumbowyg.highlight.js"></script>
	<script type="text/javascript" src="lib/trumbowyg/trumbowyg.preformatted.js"></script>

	<!--script type="text/javascript" src="lib/trumbowyg/trumbowyg.cleanpaste.min.js"></script-->
	<script src="lib/ace-builds/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
	<script src="view/js/bootstrap.min.js"></script>
	
</head>

<body>
	<nav class="navbar navbar-dark bg-primary fixed-top">
		<a class="navbar-brand" href="index.php">
			<img class="d-inline-block align-top" src="./lib/logonavbar.png" alt="" width="" height="30">
		</a>
		<?php
            require_once File::build_path(array('controller','routeurNav.php'));
        ?>
	</nav>
	<div class="container-fluid">
		<div class="row flex-xl-nowrap">
			<?php
			require_once File::build_path(array('controller','routeur.php'));
		?>
		</div>
	</div>
</body>

</html>