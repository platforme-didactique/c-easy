<main class="container ">
    <h2>Les créateurs :</h2>
    <div class="card-columns">
        <div class="card border-primary">
            <img src="lib/surprise1.jpg" class="card-img-top" style="height: 354.656px" alt="...">
            <div class="card-body ">
                <h5 class="card-title text-center">Jonathan BAUDILLON</h5>
                <p class="card-text text-center text-muted">Respo. Chinoiserie</p>
                <div class="col mx-auto text-center">
                    <h1>
                        <strong>
                            <a href="https://www.linkedin.com/in/Jonathan-baudillon" style="color: #0077B5;">
                                <i class="fab fa-linkedin"></i>
                            </a>
                            <a href="https://gitlab.com/Glorgs" style="color: #fc6d26;">
                                <i class="fab fa-gitlab"></i>
                            </a>
                        </strong>
                    </h1>
                </div>
            </div>
        </div>

        <div class="card border-primary">
            <img src="lib/surprise2.jpg" class="card-img-top" style="height: 354.656px" alt="...">
            <div class="card-body ">
                <h5 class="card-title text-center">Gabrielle CHASTAING</h5>
                <p class="card-text text-center text-muted">Respo. Kebab</p>
                <div class="col mx-auto text-center">
                    <h1>
                        <strong>
                            <a href="https://www.linkedin.com/in/gabrielle-chastaing-41a0a8107/"
                                style="color: #0077B5;">
                                <i class="fab fa-linkedin"></i>
                            </a>
                            <a href="https://gitlab.com/DemmMy?" style="color: #fc6d26;">
                                <i class="fab fa-gitlab"></i>
                            </a>
                        </strong>
                    </h1>
                </div>
            </div>
        </div>

        <div class="card border-primary">
            <img src="lib/surprise3.jpg" class="card-img-top" style="height: 354.656px" alt="...">
            <div class="card-body">
                <h5 class="card-title text-center">Adam COLAS</h5>
                <p class="card-text text-center text-muted">Respo. Glandouille</p>
                <div class="col mx-auto text-center">
                    <h1>
                        <strong>
                            <a href="https://www.linkedin.com/in/adam-colas-160045187" style="color: #0077B5;">
                                <i class="fab fa-linkedin"></i>
                            </a>
                            <a href="https://gitlab.com/fatevald" style="color: #fc6d26;">
                                <i class="fab fa-gitlab"></i>
                            </a>
                        </strong>
                    </h1>
                </div>
            </div>
        </div>
        <div class="card border-primary">
            <img src="lib/surprise4.jpg" class="card-img-top" style="height: 354.656px" alt="...">
            <div class="card-body">
                <h5 class="card-title text-center">Sylvain HUSS</h5>
                <p class="card-text text-center text-muted">Respo. Qualité</p>
                <div class="col mx-auto text-center">
                    <h1>
                        <strong>
                            <a href="https://www.linkedin.com/in/sylvain-huss/" style="color: #0077B5;">
                                <i class="fab fa-linkedin"></i>
                            </a>
                            <a href="https://gitlab.com/sylvainhuss" style="color: #fc6d26;">
                                <i class="fab fa-gitlab"></i>
                            </a>
                        </strong>
                    </h1>
                </div>
            </div>
        </div>

        <div class="card border-primary">
            <img src="lib/surprise5.jpg" class="card-img-top" style="height: 354.656px" alt="...">
            <div class="card-body">
                <h5 class="card-title text-center">Loïc MAYOL</h5>
                <p class="card-text text-center text-muted">Respo. des arts ninjas</p>
                <div class="col mx-auto text-center">
                    <h1>
                        <strong>
                            <a href="https://www.linkedin.com/in/loïc-mayol-97188b15b" style="color: #0077B5;">
                                <i class="fab fa-linkedin"></i>
                            </a>
                            <a href="https://gitlab.com/DO3B" style="color: #fc6d26;">
                                <i class="fab fa-gitlab"></i>
                            </a>
                        </strong>
                    </h1>
                </div>
            </div>
        </div>

        <div class="card border-primary">
            <img src="lib/surprise6.jpg" class="card-img-top" style="height: 354.656px" alt="...">
            <div class="card-body">
                <h5 class="card-title text-center">Marius RIDEL</h5>
                <p class="card-text text-center text-muted">Respo. Goûter</p>
                <div class="col mx-auto text-center">
                    <h1>
                        <strong>
                            <a href="https://www.linkedin.com/in/marius-ridel/" style="color: #0077B5;">
                                <i class="fab fa-linkedin"></i>
                            </a>
                            <a href="https://gitlab.com/Marius_RIDEL" style="color: #fc6d26;">
                                <i class="fab fa-gitlab"></i>
                            </a>
                        </strong>
                    </h1>
                </div>
            </div>
        </div>
    </div>
    </div>
</main>