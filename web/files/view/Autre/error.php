<main class="container mx-auto my-auto">
    <div class="row">
        <div class="mx-auto col text-center">
            <h2>Bah alors !? On s'est perdu ?</h2>
            <img src="lib/surprise7.jpg" class="img-fluid rounded-circle border border-primary" alt="Responsive image">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="mx-auto col text-center">
            <button class="btn btn-primary" onclick="window.location='index.php'">Retourner voir maman</button>
        </div>
    </div>
</main>