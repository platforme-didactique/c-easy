<main class='mx-auto mt-3'>
    <?php 
        echo '<h1>Bonjour <a class="text-primary" href="./index.php?controller=utilisateur&action=profil">'.ucfirst( explode( '.', unserialize( $_SESSION['utilisateur'] )->nom )[0] ).'</a> !</h1>'
    ?>

    <h2>Bienvenue sur C-Easy, la plateforme didactique pour apprendre le C en s'amusant.</h2>
    <br />
    <p>Vous trouverez la liste des chapitres et leurs exercices sur votre gauche.
        <br>Choisissez-en un et vous serez transportés vers le monde formidable et joyeux de la programmation en C !</p>

    <div class="text-center mt-5">
        <img class="img-fluid col-3" alt="logo ceasy" src="lib/logo.png" style />
    </div>

    <a class="fbr-0 btn btn-outline-primary"
        href="./index.php?controller=administration&action=correction-partiels">Correction des partiels</a>
</main>