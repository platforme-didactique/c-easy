<?php
    require_once "../../models/Promo.php";
    require_once "../../models/ConnectionBD.php";
    $singleton = ConnectionDB::getInstance();
    $conn = $singleton->getConnection();

    try {
        Promo::deletePromoDB($conn, $_POST['id']);
    } catch (Exception $e) {
        echo json_encode(['code'=>404, 'msg'=>'<li>'.$e->getMessage().'</li>']);
    }
     
    echo json_encode(['code'=>200, 'msg'=>"Tout va bien"]);
?>