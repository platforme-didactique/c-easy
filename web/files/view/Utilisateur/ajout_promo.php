<script src="view/js/ajoutPromo.js" type="text/javascript" charset="utf-8"></script>
<main class="container-fluid">

    <div class="mx-auto col-6 mt-3">
        <form method='post' id='formulaire'>
            <fieldset class="form-group">
                <label>Année de la promotion : </label>
                <input type="text" name="promo" id='promo' class="form-control">
            </fieldset>
            <fieldset class="form-group">
                <label>Liste d'adresses e-mail :</label>
                <textarea class="form-control" name="listeEleve" id="listeEleve" rows="3"></textarea>
            </fieldset>
            <button type="submit" id='ajoutPromo' class="btn btn-primary">Ajouter les éleves</button>
            <div class="alert alert-danger display-error mt-3" style="display: none"></div>
        </form>
    </div>
    <div id='mdp_afficher' class="mx-auto text-center col-6 mt-3">
        <table class="table" id="table">
            <thead>
                <tr>
                    <th scope="col">Identifiant</th>
                    <th scope="col">Mot de passe</th>
                </tr>
            </thead>
            <tbody id='contenu'>
            </tbody>
        </table>
        <button type='button' id='telecharger' class="btn btn-success"> <i class="fas fa-download"></i> Télécharger en
            PDF <i class="fas fa-download"></i></button>
    </div>
</main>