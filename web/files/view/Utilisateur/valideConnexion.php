<?php
    session_start();
    require_once "../../models/Utilisateur.php";
    require_once "../../models/ConnectionBD.php";
    $singleton = ConnectionDB::getInstance();
    $conn = $singleton->getConnection();

    $errorMSG = "";

    /* EMAIL */
    if ( !strpos($_POST["email"], "univ-amu.fr" ) ) {
        $errorMSG .= "<li>Adresse e-mail invalide.</li>";
    } 
    else {
        $email = $_POST["email"];
    }

    $password = $_POST["password"];

    if(empty($errorMSG)){
        try{
            $_SESSION['utilisateur'] = serialize(Utilisateur::verificationConnection($conn, $email,$password));
            echo json_encode(['code'=>200, 'msg'=>"Tout va bien"]);
        }
        catch(Exception $e){
            echo json_encode(['code'=>404, 'msg'=>'<li>'.$e->getMessage().'</li>']);
        }
    }
    else{
        echo json_encode(['code'=>404, 'msg'=>$errorMSG]);
    }
?>