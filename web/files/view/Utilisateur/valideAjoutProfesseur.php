<?php
    session_start();
    require_once "../../models/Utilisateur.php";
    require_once "../../models/ConnectionBD.php";
    require_once "../../models/Promo.php";
    $singleton = ConnectionDB::getInstance();
    $conn = $singleton->getConnection();

    $errorMSG = "";

    if ( $_POST["mail"]==NULL ) {
        $errorMSG .= "<li>Merci de rentrer une adresse email.</li>";
    }
    else{
        if(!strpos($_POST["mail"], "@univ-amu.fr" )){
            $errorMSG .= "<li>L'adresse email doit finir par @univ-amu.fr.</li>";
        }else{
            $mail=$_POST["mail"];
        }
    }

    if($_POST['mdp']==NULL){
        $errorMSG .= "<li>Veuillez entrer un mot de passe.</li>";
    }
    else{
        $mdp=$_POST['mdp'];
    }

    if(empty($errorMSG)){
        try{
            //Ajout du professeur dans la BDD
            $array=array();
            $array[]=array("user"=>$mail, "mdp"=>Utilisateur::createUserAdminDB($conn, $mail,$mdp));
            echo json_encode(array("code"=>200, "users"=>$array));
        }
        catch(Exception $e){
            echo json_encode(['code'=>404, 'msg'=>'<li>'.$e->getMessage().'</li>']);
        }
    }
    else{
        echo json_encode(['code'=>404, 'msg'=>$errorMSG]);
    }

?>