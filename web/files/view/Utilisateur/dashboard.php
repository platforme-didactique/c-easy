<script src="view/js/dashboard.js" type="text/javascript" charset="utf-8"></script>
<main class="container" id="dashboard">

    <ul class="nav nav-tabs nav-fill mt-3" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" href="#exos" id="exos-tab" data-toggle="tab" aria-controls="exos"
                aria-selected="true">Exercices</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#users" id="users-tab" data-toggle="tab" aria-controls="users"
                aria-selected="false">Utilisateurs</a>
        </li>
    </ul>

    <div class="tab-content border border-top-0  py-3 px-3 h-100">

        <div class="tab-pane fade show active" id="exos" role="tabpanel" aria-labelledby="exos-tab">

            <div class="text-center mb-3">
                <a class="btn btn-primary " href="index.php?controller=chapitre&action=ajoutchapitre">Ajouter
                    un chapitre</a>
                <?php
                    try{
                    $chapitre=Chapitre::getAllChapitreDB($conn);
                     echo '<a class="btn btn-primary " id="addEx" href="index.php?controller=exercice&action=ajoutexercice">Ajouter
                    un exercice</a>';
                    }
                    catch(Exception $e){} ?>
            </div>

            <div id="accordion">
                <?php
                try{
                    $chapitre=Chapitre::getAllChapitreDB($conn);
                    foreach ($chapitre as $key => $chap) {
                        echo '<div class="card">
                        <div class="card-header ">
                        <h5 class="float-left card-link w-75" data-toggle="collapse" href="#collapseC'.$chap->id.'">Chapitre : '.$chap->nom .'
                        </h5>
                        <span class="float-right">
                        
                        <a class="petit-texte btn btn-outline-warning btn-circle" href="index.php?controller=chapitre&action=modifierchapitre&id='.$chap->id.'">
                            <i class="fas fa-edit"></i>
                        </a>

                        <button class="petit-texte btn btn-outline-danger btn-circle" onclick="supprimerChapitre('.$chap->id.')">
                            <i class="fas fa-times"></i>
                        </button>

                        </span>
                        </div>
                        <div id="collapseC'.$chap->id.'" class="collapse" data-parent="#accordion">
                        <div class="card-body">
                        <ul class="card-text">';

                        try{
                            $exercice = Chapitre::exerciceFromChapitre($conn,$chap->id);
                            foreach ($exercice as $key=>$exo){
                                echo'<li class="my-2">Exercice : '.$exo->titre.'
                                <span class="float-right">
                        
                                <a class="petit-texte btn btn-warning btn-circle" href="index.php?controller=exercice&action=modifierexercice&idchap='.$chap->id.'&id='.$exo->id.'">
                                    <i class="fas fa-edit"></i>
                                </a>

                                <button class="petit-texte btn btn-danger btn-circle" onclick="supprimerExo('.$chap->id.','.$exo->id.')">
                                    <i class="fas fa-times"></i>
                                </button>

                                </span>
                                </li>';
                            }
                        }
                        catch(Exception $e){
                            echo'<li>'.$e->getMessage().'</li>';
                        }
                        echo'</ul>
                            </div>
                            </div>
                            </div>';
                    }
                }
                catch(Exception $e){
                    echo $e->getMessage();
                }
            ?>

            </div>

        </div>

        <div class="tab-pane fade" id="users" role="tabpanel" aria-labelledby="users-tab">

            <div class="text-center mb-3">
                <a class="btn btn-primary " href="index.php?controller=utilisateur&action=ajoutpromo">Ajouter une
                    promotion</a>
                <a class="btn btn-primary " href="index.php?controller=utilisateur&action=ajouteutilisateur">Ajouter un utilisateur</a>
            </div>

            <div id="accordion">
                <?php
                    try{
                        $promotion=Promo::getAllPromoDB($conn);
                        foreach ($promotion as $key => $promo) {
                            echo '<div class="card">
                            <div class="card-header ">
                            <h5 class="float-left card-link w-75" data-toggle="collapse" href="#collapseP'.$promo->id.'">Promotion '.$promo->annee .'
                            </h5>
                            <span class="float-right">
                            
                            <button class="petit-texte btn btn-outline-danger btn-circle" onclick="supprimerPromo('.$promo->id.')">
                                <i class="fas fa-times"></i>
                            </button>

                            </span>
                            </div>
                            <div id="collapseP'.$promo->id.'" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                            <ul class="card-text">';

                            try{
                                $eleves = Promo::eleveFromPromo($conn,$promo->id);
                                foreach ($eleves as $key=>$eleve){
                                    echo'
                                    <form method="post" action="./index.php?controller=utilisateur&action=profil">
                                    <input type="text" id="idEleve" name="idEleve" value="'.$eleve->id.'" hidden></input>
                                    <input type="text" id="nomEleve" name="nomEleve" value="'.ucfirst( explode( '.', $eleve->nom )[0] ).'" hidden></input>
                                    <li class="my-2">'.$eleve->nom.'
                                    <span class="float-right">
                                    <button class="petit-texte btn btn-success btn-circle" type="submit">
                                        <i class="fas fa-eye"></i>
                                    </button>
                                    </span>
                                    </li>
                                    </form>';
                                }
                            }
                            catch(Exception $e){
                                echo'<li>'.$e->getMessage().'</li>';
                            }
                            echo'</ul>
                                </div>
                                </div>
                                </div>';
                        }
                    }
                    catch(Exception $e){
                        echo $e->getMessage();
                    }
                    
                    echo '<div class="card">
                        <div class="card-header ">
                        <h5 class="float-left card-link w-75" data-toggle="collapse" href="#collapseAdmin">Administrateurs</h5>
                        </div>
                        <div id="collapseAdmin" class="collapse" data-parent="#accordion">
                        <div class="card-body">
                        <ul class="card-text">';

                        try{
                            $admins = Utilisateur::getAllAdminDB($conn);
                            foreach ($admins as $key=>$admin){
                                echo'<li class="my-2">'.$admin->nom.'</li>';
                            }
                        }
                        catch(Exception $e){
                            echo'<li>'.$e->getMessage().'</li>';
                        }
                        echo'</ul>
                            </div>
                            </div>
                            </div>';

                ?>

            </div>

        </div>
    </div>

</main>