<?php
session_start();
require_once "../../models/Utilisateur.php";
require_once "../../models/ConnectionBD.php";
require_once "../../models/Promo.php";
$singleton = ConnectionDB::getInstance();
$conn = $singleton->getConnection();

$errorMSG = "";

if ( $_POST["mail"]==NULL ) {
    $errorMSG .= "<li>Il faut entrer l'adresse email de l'élève.</li>";
}
else{
    if(!strpos($_POST["mail"], "@etu.univ-amu.fr" )){
        $errorMSG .= "<li>Uniquement les adresses universitaires sont acceptées. Merci de rentrer des adresses e-mails finissant par <b>@etu.univ-amu.fr</b>.</li>";
    }else{
        $mail=$_POST["mail"];
        $nomP=$_POST["promo"];
    }
}

if(empty($errorMSG)){
    try{
        //Récupère l'id de la promo
        $promo=Promo::getPromoByAnnee($conn,$nomP);
        //Ajout des élèves dans la BDD
        $array=array();
        $array[]=array("user"=>$mail, "mdp"=>Utilisateur::createUserDB($conn, $mail, $promo->id));
        echo json_encode(array("code"=>200, "users"=>$array));
    }
    catch(Exception $e){
        echo json_encode(['code'=>404, 'msg'=>'<li>'.$e->getMessage().'</li>']);
    }
}
else{
    echo json_encode(['code'=>404, 'msg'=>$errorMSG]);
}


?>