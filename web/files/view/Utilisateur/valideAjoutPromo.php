<?php
    session_start();
    require_once "../../models/Utilisateur.php";
    require_once "../../models/ConnectionBD.php";
    require_once "../../models/Promo.php";
    $singleton = ConnectionDB::getInstance();
    $conn = $singleton->getConnection();

    $errorMSG = "";

    if ( $_POST["nomPromo"]==NULL ) {
        $errorMSG .= "<li>Il est obligatoire de renseigner l'année de la promotion.</li>";
    } 
    else {
        $nomP = $_POST["nomPromo"];
    }
    if ( $_POST["liste"]==NULL ) {
        $errorMSG .= "<li>Il faut entrer les adresses emails des élèves.</li>";
    }
    else{
        $liste = $_POST["liste"];
        $array=explode(";", $liste);
        $tab_liste=array_filter($array);
        foreach ($tab_liste as $key => $value) {
            if(!strpos($value, "@etu.univ-amu.fr" )){
                if(!strpos($errorMSG,"finir par")){
                    $errorMSG .= "<li>Uniquement les adresses universitaires sont acceptées. Merci de rentrer des adresses e-mails finissant par <b>@etu.univ-amu.fr</b>.</li>";
                }
            }
        }
    }

    if(empty($errorMSG)){
        try{
            //Creation de la promo
            Promo::createPromoDB($conn,$nomP);
            //Récupère l'id de la promo
            $promo=Promo::getPromoByAnnee($conn,$nomP);
            //Ajout des élèves dans la BDD
            $array=array();
            foreach ($tab_liste as $key => $value) {
                $array[]=array("user"=>$value, "mdp"=>Utilisateur::createUserDB($conn, $value, $promo->id));
            }
            echo json_encode(array("code"=>200, "users"=>$array));
        }
        catch(Exception $e){
            echo json_encode(['code'=>404, 'msg'=>'<li>'.$e->getMessage().'</li>']);
        }
    }
    else{
        echo json_encode(['code'=>404, 'msg'=>$errorMSG]);
    }
?>