<link rel="stylesheet" href="style/connection.css">
<script src="view/js/connexion.js" type="text/javascript" charset="utf-8"></script>
<form method="post" class="form-connection align-middle" role="login">
  <div class="text-center mb-4">
    <img class="mb-4" src="./lib/logo.png" alt="" width="80" height="80">
    <h1 class="h3 mb-3 font-weight-normal">Bienvenue sur C-Easy !</h1>
    <p>Connectez-vous pour apprécier vos deux prochaines heures de TP (ou plus). </p>
  </div>
  <div class="form-label-group">
    <input type="text" name="email" id='email' class="form-control" placeholder="prenom.nom@etu.univ-amu.fr" required
      autofocus>
    <label for="email">Adresse e-mail universitaire</label>

  </div>
  <div class="form-label-group">
    <input type="password" name="password" id="password" class="form-control" placeholder="prenom.nom@etu.univ-amu.fr"
      required autofocus>
    <label for="password">Mot de passe</label>
  </div>
  <br><br>

  <button type="submit" name="go" id='submit' class="btn btn-lg btn-primary btn-block">Connexion</button>

  <div class="alert alert-danger display-error mt-3" style="display: none"></div>

  <div class="mt-5 mb-3 text-muted text-center">
    &copy;
    <span id="copyright">
      <script>
        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
      </script>
    </span>
  </div>

  </div>


</form>

<footer class="bg-primary"><a href="./index.php?controller=utilisateur&action=credits">Crédits</a></footer>