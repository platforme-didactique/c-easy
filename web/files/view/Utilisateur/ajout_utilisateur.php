<script src="view/js/ajoutUtilisateur.js" type="text/javascript" charset="utf-8"></script>
<main class="container-fluid">

    <div id='formulaire' class="mt-3 row">
    <?php
                try{
                    $promo=Promo::getAllPromoDB($conn);
        echo'<div class="col-sm-6">
            <div class="card">

                <div class="card-header">
                    <h5 class="card-title">Ajouter un élève</h5>
                </div>

                <div class="card-body card-hmax">

                    <form method="post">
                        <fieldset class="form-group">
                            <label>Choix de la promotion : </label>
                            <select id="promo" name="my_html_select_box">';

                            
                            foreach ($promo as $key => $value) {
                                echo "<option selected=\"yes\">".$value->annee."</option>";
                            }

                        echo'    </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>Adresse e-mail :</label>
                            <input type="text" name="mailEleve" id="mailEleve" class="form-control">
                        </fieldset>
                        <button type="submit" id="ajoutEleve" class="btn btn-primary">Ajouter l\'élève</button>
                        <div class="alert alert-danger display-error mt-3" style="display: none"></div>
                    </form>

                </div>

            </div>
        </div>';
        }
                catch(Exception $e){echo '<div class="col-sm-3"></div>';}
    ?>

        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Ajouter un professeur</h5>
                </div>

                <div class="card-body card-hmax">

                    <form method='post'>
                        <fieldset class="form-group">
                            <label>Adresse e-mail :</label>
                            <input type="text" name="mailProf" id='mailProf' class="form-control">
                        </fieldset>
                        <fieldset class="form-group">
                            <label>Mot de passe :</label>
                            <input type="password" name="mdpProf" id='mdpProf' class="form-control">
                        </fieldset>
                        <button type="submit" id='ajourProf' class="btn btn-primary">Ajouter le professeur</button>
                        <div class="alert alert-danger display-error mt-3" style="display: none"></div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div id='mdp' class="mx-auto text-center col-6 mt-3">
        <table class="table" id='table'>
            <thead>
                <tr>
                    <th scope="col">Identifiant</th>
                    <th scope="col">Mot de passe</th>
                </tr>
            </thead>
            <tbody id='contenu'>
            </tbody>
        </table>
        <button type='button' id='telecharger' class="btn btn-success"> <i class="fas fa-download"></i> Télécharger en
            PDF <i class="fas fa-download"></i></button>
    </div>

</main>