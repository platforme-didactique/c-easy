<script src="view/js/profil.js" type="text/javascript" charset="utf-8"></script>
<main class="container">
    <?php
        if(!isset($_POST['idEleve'])){
            $id = unserialize($_SESSION['utilisateur'])->id;
            echo '<h5>Mon profil :</h5>';
        }
        else{
            $id = $_POST['idEleve'];
            echo '<h5>Profil de '.$_POST['nomEleve'].' :</h5>';
        }

        echo '<div class="card-columns mx-2 my-2">';

        try{
            $chapitre=Chapitre::getAllChapitreDB($conn);
            
            foreach ($chapitre as $key => $chap) {
                if($chap->visible == 1){
                    $realise = Utilisateur::getExosValideByChapitre($conn, $id, $chap->id);
                    try{
                        $total = Chapitre::exerciceFromChapitre($conn,$chap->id);

                        foreach ($total as $key=>$exo){
                            if($exo->visible != 1)
                                unset($total[$key]);
                        }
                        foreach ($realise as $key=>$exo){
                            if($exo->visible != 1)
                                unset($realise[$key]);
                        }

                        if( count($total) != 0){
                            $percent = number_format((( count($realise) / count($total) ) * 100), 0);
                        }
                        else{
                            $percent = 0;
                        }

                        echo '<div class="card">
                            <h5 class="card-header">'.$chap->nom .' <span class="float-right '.  ( ($percent == 100)?'text-success':'text-primary') .'">'. $percent .'%</span></h5>
                            <div class="card-body">
                                <ul class="card-text">';
                        foreach ($total as $key=>$exo){
                            if($exo->visible == 1){
                                echo'<li>'.$exo->titre;
                                
                                if(in_array($exo,$realise)){
                                    $real = Realise::getRealiseDB($conn, $exo->id, $chap->id, $id);
                                    $code = preg_replace("/[\n\r]/","<br/>",$code);
                                    $code = preg_replace("/  /","<span class='ml-3'/>", $code);
                                    
                                    echo '<i class="pointer float-right text-success fas fa-check" data-toggle="modal" data-target="#modal'.$chap->id.$exo->id.'"></i>';
                                    ?>
                                    <script type="text/javascript" charset="utf-8">
                                        $(document).ready(function () {
                                            var editor = ace.edit("editor<?php echo $chap->id.$exo->id; ?>");
                                            editor.setOptions({
                                                selectionStyle: "text",
                                                highlightSelectedWord: true,
                                                fontSize: 15,
                                                mode: "ace/mode/c_cpp",
                                                theme: "ace/theme/monokai",
                                                readOnly: true
                                            });
                                        });
                                    </script>

                                    <div class="modal fade" id="modal<?php echo $chap->id.$exo->id; ?>" tabindex="-1" role="dialog" aria-labelledby="#labelModal<?php echo $chap->id.$exo->id; ?>" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="labelModal<?php echo $chap->id.$exo->id; ?>"><?php echo "Nombre d'essai(s) : ". $real->nb_essai; ?></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div id="editor<?php echo $chap->id.$exo->id; ?>" class="modal-body edit75"><?php echo htmlspecialchars($real->code); ?></div>
                                                <div class="modal-footer">
                                                    <?php
                                                        $st = strtotime($real->temps_debut);
                                                        $dt1 = new DateTime("@$st");
                                                        $st = strtotime($real->temps_valide);
                                                        $dt2 = new DateTime("@$st");
                                                        
                                                        echo $dt1->diff($dt2)->format("Exercice réalisé en : %d jour(s), %h heure(s), %i minute(s) et %s seconde(s).");
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                else{
                                    echo '<i class="float-right text-danger fas fa-times"></i>';
                                }
                                
                                echo'</span></li>';
                            }
                        }
                        echo'        </ul>
                            </div>
                        </div>';
                    }
                    catch(Exception $e){
                        continue;
                    }
                }
            }
        }
        catch(Exception $e){
            echo $e->getMessage();
        }
    ?>
    </div>
</main>