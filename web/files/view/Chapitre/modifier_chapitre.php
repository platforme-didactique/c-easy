<script src="view/js/modifierChapitre.js" type="text/javascript" charset="utf-8"></script>
<main class="container-fluid">
    <div class="mt-3 mx-auto col-6">
        <div class="col-xs-12 well">
            <form method='post'>
                <fieldset class="form-group">
                    <label>Nom du chapitre : </label>
                    <input type="text" name="chap" id='chap' class="form-control"
                        <?php echo 'value="'. $chapitre->nom .'"'; ?>>
                </fieldset>
                <fieldset class="form-group">
                    <label>Recommandations :</label>
                    <div id="trumbowyg-editor" name="content" name="aide" id="aide"><?php echo $chapitre->indice; ?></div>
                </fieldset>

                <fieldset class="form-group">
                    <input type="checkbox" id="visible" name="visible" <?php if($chapitre->visible){echo "checked";} ?>>
                    <label for="visible">Visible</label>
                </fieldset>
                <button type="button"
                    <?php echo 'onclick="modifierChapitre('.$chapitre->id.')"';?>class="btn btn-primary">Modifier le chapitre</button>

                <div class="alert alert-danger display-error mt-3" style="display: none"></div>
            </form>
        </div>
    </div>
</main>