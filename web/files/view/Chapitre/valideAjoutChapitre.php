<?php
    session_start();
    require_once "../../models/Chapitre.php";
    require_once "../../models/ConnectionBD.php";
    $singleton = ConnectionDB::getInstance();
    $conn = $singleton->getConnection();

    $errorMSG = "";


    if ( $_POST["nomChapitre"]==NULL ) {
        $errorMSG .= "<li>Il faut un nom de chapitre</li>";
    } 
    else {
        $nomC = $_POST["nomChapitre"];
        $indice=$_POST["aide"];
        $visible=$_POST["visible"];
    }


    if(empty($errorMSG)){
        try{
            Chapitre::createChapitreDB($conn,$nomC,$indice,$visible);
            echo json_encode(['code'=>200, 'msg'=>"Tout va bien"]);
        }
        catch(Exception $e){
            echo json_encode(['code'=>404, 'msg'=>'<li>'.$e->getMessage().'</li>']);
        }
    }
    else{
        echo json_encode(['code'=>404, 'msg'=>$errorMSG]);
    }
?>