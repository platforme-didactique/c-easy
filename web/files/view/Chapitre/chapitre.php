<link rel="stylesheet" href="style/sidebar.css">

<main class="col-2">
	<div class="row nav-side-menu">

		<div class="menu-list">

			<ul id="menu-content" class="menu-content collapse out">

				<!-- A Mettre dans la boucle-->
				<?php
				try{
					$tab_chapitre=Chapitre::getAllChapitreDB($conn);
					foreach ($tab_chapitre as $key => $chap) {
						if($chap->visible ==1){
							
							try{
								$exo = Chapitre::exerciceFromChapitre($conn,$chap->id);
								echo "<li data-toggle=\"collapse\" data-target=\"#detailsCollapse$chap->id\" class=\"chap collapsed\">
								<i class=\"fas fa-book-medical\"></i><i class=\"fas fa-book-open\"></i> $chap->nom
								</li>";

								echo "<ul class=\"sub-menu collapse\" id=\"detailsCollapse$chap->id\">";
								foreach ($exo as $key => $exo){
									if($exo->visible ==1){
										echo '<li onclick=\'window.location="index.php?controller=exercice&action=execute&idExo='.rawurlencode($exo->id).'&idChapitre='.rawurlencode($exo->id_chap).'";\'>';
										echo "<span class='pl-3'>  <i class=\"pl-2 fas fa-file-signature\"></i> $exo->titre</span>";
										echo '</li>';
									}
								}
								echo "</ul>";
							}
							catch(Exception $e){
								continue;
							}
							
						}
					}
				}
				catch(Exception $e){
					echo '<li>'.$e->getMessage().'</li>';
				}
				?>

				<!-- Fin boucle-->

			</ul>

		</div>
	</div>
	<div class="row nav-side-footer">
		<footer class="bg-primary"><a href="./index.php?controller=utilisateur&action=credits">Crédits</a></footer>
	</div>
</main>