<?php
    require_once "../../models/Chapitre.php";
    require_once "../../models/ConnectionBD.php";
    $singleton = ConnectionDB::getInstance();
    $conn = $singleton->getConnection();

    try {
        Chapitre::deleteChapitreDB($conn, $_POST['id']);
    } catch (Exception $e) {
        echo json_encode(['code'=>404, 'msg'=>'<li>'.$e->getMessage().'</li>']);
    } 
    echo json_encode(['code'=>200, 'msg'=>"Tout va bien"]);
?>