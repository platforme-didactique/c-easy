<script src="view/js/ajoutChapitre.js" type="text/javascript" charset="utf-8"></script>
<main class="container-fluid">
    <div class="mt-3 mx-auto col-6">
        <div class="col-xs-12 well">
            <form method='post'>
                <fieldset class="form-group">
                    <label>Nom du chapitre : </label>
                    <input type="text" name="chap" id='chap' class="form-control">
                </fieldset>
                <fieldset class="form-group">
                    <label>Recommandations :</label>
                    <div id="trumbowyg-editor" name="content" name="aide" id="aide"></div>
                </fieldset>

                <fieldset class="form-group">
                    <input type="checkbox" id="visible" name="visible" checked>
                    <label for="visible">Visible</label>
                </fieldset>
                <button type="submit" id='ajoutChapitre' class="btn btn-primary">Ajouter le chapitre</button>

                <div class="alert alert-danger display-error mt-3" style="display: none"></div>
            </form>
        </div>
    </div>
</main>