<script src="view/js/modifierExercice.js" type="text/javascript" charset="utf-8"></script>
<script src="view/js/affichage_formulaire.js" type="text/javascript" charset="utf-8"></script>

<main class="container-fluid">

  <div class="my-3 mx-auto col-6">
    <form method='post'>
      <div id='partieA'>
        <fieldset class="form-group">
          <label>Chapitre correspondant : </label>
          <select id='chapitre' name="my_html_select_box">
            <?php
                    
                    foreach ($allchapitre as $key => $value) {
                        if($value->nom != $chapitre->nom)
                        {
                            echo "<option selected=\"yes\">".$value->nom."</option>";
                        }
                        
                        
                    }

                    echo "<option selected=\"yes\">".$chapitre->nom."</option>";
                    ?>
          </select>
        </fieldset>
        <input type="hidden" id="recup_id" <?php echo 'value="'. $exercice->id .'"'; ?>>
        <fieldset class="form-group">
          <label>Nom de l'exercice : </label>
          <input type="text" name="nomExo" id='nomExo' class="form-control"
            <?php echo 'value="'. $exercice->titre .'"'; ?>>
        </fieldset>

        <fieldset class="form-group">
          <label>Consigne de l'exercice : </label>
          <div id="trumbowyg-editor" name="content"><?php echo $exercice->consigne; ?></div>
        </fieldset>
      </div>

      <div id='partieB'>
        <fieldset class="form-group">
          <input type="checkbox" id="visible" name="visible" <?php if($exercice->visible){echo "checked";} ?>>
          <label for="visible">Visible</label>
        </fieldset>
        <fieldset class="form-group">
          <label>Dépendances : </label>
          <div class="w-100" style="height:6rem">
            <div id="editor" class="card-body"><?php echo htmlspecialchars($exercice->dependances); ?></div>
          </div>
        </fieldset>

        <fieldset class="form-group">
          <label>Main : </label>
          <div class="w-100" style="height:calc(100vh - 22rem)">
            <div id="editor2" class="card-body"><?php echo htmlspecialchars($exercice->main); ?></div>
          </div>
        </fieldset>

      </div>

      <div id='partieC'>

        <label>Jeux de test :</label>
        <fieldset class="form-group">
          <div id="lesES">
            <?php
              foreach ($lesTests as $key => $test) {
                echo'<div id="ES'.($key+1).'" class=" input-group mb-3">
                <input type="text" class="form-control" name="entree'.($key+1).'" id="entree'.($key+1).'" placeholder="Entrée" aria-label="Entrée" aria-describedby="basic-addon2" value="'.$test->entree.'">
                <input type="text" class="form-control" name="sortie'.($key+1).'" id="sortie'.($key+1).'" placeholder="Sortie" aria-label="Sortie" aria-describedby="basic-addon2" value="'.$test->sortie.'">
              </div>';
              }
              
            ?>

          </div>
          <button type="button" class="btn btn-success btn-circle float-right" id="addES"><i
              class="fas fa-plus"></i></button>
        </fieldset>


        <fieldset class="form-group">
          <label>Correction de l'exercice :</label>
          <div class="card" style="height: calc(100vh - 22rem)">
            <div id="editor3" class="card-body"><?php echo $exercice->corrige; ?></div>
          </div>
        </fieldset>
      </div>

      <input type="button" id='suivantA' class="btn btn-primary" value="Suivant">
      <input type="button" id='precedentA' class="btn btn-primary" value="Précedent">
      <input type="button" id='suivantB' class="btn btn-primary" value="Suivant">
      <input type="button" id='precedentB' class="btn btn-primary" value="Précedent">
      <button type="button" id='modifierExercice' class="btn btn-primary"
        onclick="modifierExo(<?php echo $chapitre->id.', '.$exercice->id ;?>)">Modifier l'exercice</button>
      <div class="alert alert-danger display-error mt-3" style="display: none"></div>
    </form>
  </div>
</main>