<script src="//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML"></script>
<script>
	MathJax.Hub.Config({
	tex2jax: {
		inlineMath: [
			['$', '$'],
			['\\(', '\\)']
		]
	},
		CommonHTML: { linebreaks: { automatic: true } },
	"HTML-CSS": { linebreaks: { automatic: true } },
		SVG: { linebreaks: { automatic: true } }
});
</script>

<main class="col-10 mt-3">

	<div class="container-fluid mx-auto">
		<div class="row">
			<div class="col-7">
				<div class="card" id='reponse'>
					<div class="card-header">
						<b>Réponse</b> <i class="float-right fas fa-code"></i>
					</div>
					<div id="editor" class="card-body"></div>
				</div>
			</div>
			<div class="col-5">
				<div class="row">
					<div class="card w-100" id='enonce'>
						<div class="card-header">
							<?php 
									echo "<b>" . $exercice->titre . "</b>";
									if($realise_existe){
										if($realise->valide ==1){
											echo '<span id="valideIcone" class="float-right text-success ">Validé <i class="fas fa-check"></i></span>';
										}
										else{
											echo '<span id="valideIcone" class="float-right text-danger ">Non validé <i class="fas fa-times"></i></span>';
										}
									}
									else{
										echo '<span id="valideIcone" class="float-right text-danger ">Non validé <i class="fas fa-times"></i></span>';
									}
								?>
						</div>
						<div class="card-body">
							<?php echo $exercice->consigne; ?>
						</div>
					</div>
				</div>

				<div class="row mt-3">
					<div class="card w-100" id='output'>
						<div class="card-header">
							<b>Sortie</b> <i class="float-right fas fa-cogs"></i>
						</div>
						<div class="card-body justify-content-center">
							<div id="collapseText">
							</div>
						</div>
						<div class="card-footer text-center">
							<button class="btn btn-warning" id="compileButton"
								onclick="compile(<?php echo unserialize($_SESSION['utilisateur'])->id ?>,<?php echo $exercice->id_chap ?>,<?php echo $exercice->id ?>)">Compiler</button>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</main>
<script src="compiler/compilateur.js"></script>