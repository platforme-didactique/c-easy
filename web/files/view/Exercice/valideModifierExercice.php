<?php
    session_start();
    require_once "../../models/Chapitre.php";
    require_once "../../models/JeuTest.php";
    require_once "../../models/Exercice.php";
    require_once "../../models/ConnectionBD.php";
    $singleton = ConnectionDB::getInstance();
    $conn = $singleton->getConnection();

    $errorMSG = "";
    if ( $_POST["nomExo"]==NULL ) {
        $errorMSG .= "<li>Il est obligatoire de renseigner le nom de l'exercice.</li>";
    } 
    else {
        $nomE = $_POST["nomExo"];
    }

    if ( $_POST["consigne"]==NULL ) {
        $errorMSG .= "<li>Il est obligatoire de renseigner une consigne.</li>";
    } 
    else {
        $consigne = $_POST["consigne"];
    }

    if ( $_POST["dependance"]==NULL ) {
        $errorMSG .= "<li>Veuillez insérer des dépendances.</li>";
    } 
    else {
        $dependances=$_POST["dependance"];
    }

    if ( $_POST["code"]==NULL ) {
        $errorMSG .= "<li>Merci de renseigner le code.</li>";
    } 
    else {
        $main=$_POST["code"];
    }

    if ( json_decode($_POST["ES"])=="" ) {
        $errorMSG .= "<li>Pour valider votre exercice, au moins un jeu de test est nécessaire.</li>";
    } 
    else {
        $ES=json_decode($_POST["ES"]);
    }

    $id_chap = $_POST['id_chap'];
    $id = $_POST['id'];
    $nomC = $_POST["titreChap"];
    $corrige=$_POST["correction"];
    $visible=$_POST["visible"];



    if(empty($errorMSG)){
        try{
            //Récupérer id du chapitre
            $chapitre = Chapitre::getChapitreByNom($conn,$nomC);
            //Ajouter exercice et jeu de test

            if($chapitre->id != $id_chap && Exercice::exerciceExisteByNom($conn, $chapitre->id, $nomE)) throw new Exception("Il y a déjà un exercice dans ce chapitre portant ce nom.", 2);
            
            JeuTest::deleteAllJeuTestFromExoDB($conn, $id_chap, $id);
            Exercice::updateExerciceDB($conn, $id_chap, $id, $chapitre->id, $nomE, $consigne, $corrige, $dependances, $main, $visible);
            usleep(250000);
            $nouvel_exo = Exercice::getExerciceByTitre($conn, $chapitre->id, $nomE);
            

            foreach ($ES as $key => $es) {
                JeuTest::createJeuTestDB($conn, $chapitre->id, $nouvel_exo->id, $es->entree, $es->sortie);
            }
            
            //passage des paramètres        
            echo json_encode(['code'=>200, 'msg'=>"Tout va bien"]);
        }
        catch(Exception $e){
            echo json_encode(['code'=>404, 'msg'=>'<li>'.$e->getMessage().'</li>']);
        }
    }
    else{
        echo json_encode(['code'=>404, 'msg'=>$errorMSG]);
    }
?>