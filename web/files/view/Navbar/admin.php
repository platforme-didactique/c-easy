<script src="view/js/deconnexion.js" type="text/javascript" charset="utf-8"></script>
<div class='my-2 my-sm-0'>
    <button class="btn btn-danger"
        onclick='window.location = "./index.php?controller=utilisateur&action=dashboard";'>Administration <i
            class="fas fa-users-cog"></i></button>
    <button class="btn btn-outline-light" id='deconnexion'>Déconnexion <i class="fas fa-sign-out-alt"></i></button>
</div>