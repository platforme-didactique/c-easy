$(document).ready(function () {
    $('#mdp_afficher').hide();
    $('#ajoutPromo').click(function (e) {
        e.preventDefault();


        var nomPromo = $("#promo").val();
        var liste = $("#listeEleve").val();

        $.ajax({
            type: "POST",
            url: "view/Utilisateur/valideAjoutPromo.php",
            dataType: "json",
            data: {
                nomPromo: nomPromo,
                liste: liste
            },
            success: function (data) {
                if (data.code == "200") {
                    $('#mdp_afficher').show();
                    $('#formulaire').hide();
                    data.users.forEach(function (utilisateur) {
                        $('#table > tbody:last').append("<tr><td>" + utilisateur.user + "</td><td>" + utilisateur.mdp + "</td></tr>");
                    });
                } else {
                    $(".display-error").html("<ul>" + data.msg + "</ul>");
                    $(".display-error").css("display", "block");
                }
            }
        });

    });



    $('#telecharger').click(function (e) {
        var pdf = new jsPDF('p', 'pt');
        source = $('#mdp_afficher').html();
        console.log(source);
        var elementHandler = {
            '#telecharger': function (element, renderer) {
                return true;
            }
        };
        margins = {
            top: 70,
            bottom: 60,
            left: 60,
            width: 522
        };

        pdf.setProperties({
            title: 'Liste des élèves avec leurs mot de passe'
        });

        pdf.fromHTML(
            source,
            margins.left,
            margins.top, {
                'width': margins.width,
                'elementHandlers': elementHandler
            },
            function (dispose) {
                pdf.save('Acces_eleves_ceasy.pdf');
            }, margins);





    });

});