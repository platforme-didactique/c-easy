$(document).ready(function() {
    $('#mdp').hide();
    $('#ajoutEleve').click(function(e){
      e.preventDefault();

      var promo =  $('#promo option:selected').text();
      var mail = $("#mailEleve").val();
      $.ajax({
          type: "POST",
          url: "view/Utilisateur/valideAjoutEleve.php",
          dataType: "json",
          data: {promo:promo, mail:mail},
          success : function(data){
              if (data.code == "200"){
                $('#mdp').show();
                $('#formulaire').hide();
                
                data.users.forEach(function(utilisateur){
                    console.log("hello");
                    $('#table > tbody:last').append("<tr><td>"+utilisateur.user+"</td><td>"+utilisateur.mdp+"</td></tr>");
 
                });
              } else {
                  $(".display-error").html("<ul>"+data.msg+"</ul>");
                  $(".display-error").css("display","block");
              }
          },
         
      });
    });

    $('#ajourProf').click(function(e){
        e.preventDefault();
  
        var mail = $("#mailProf").val();
        var mdp=$('#mdpProf').val();
        $.ajax({
            type: "POST",
            url: "view/Utilisateur/valideAjoutProfesseur.php",
            dataType: "json",
            data: {mail:mail, mdp:mdp},
            success : function(data){
                if (data.code == "200"){
                    $('#mdp').show();
                    $('#formulaire').hide();
                    data.users.forEach(function(utilisateur){
                        $('#table > tbody:last').append("<tr><td>"+utilisateur.user+"</td><td>"+utilisateur.mdp+"</td></tr>");
                    });
                 
                } else {
                    $(".display-error").html("<ul>"+data.msg+"</ul>");
                    $(".display-error").css("display","block");
                }
            },

            error : function(data){
                console.log(data);
            }
        });
    });

    $('#telecharger').click(function(e){
        var pdf = new jsPDF('p','pt');
        // source can be HTML-formatted string, or a reference
        // to an actual DOM element from which the text will be scraped.
        source = $('#mdp').html();
        console.log(source);
        // we support special element handlers. Register them with jQuery-style 
        // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
        // There is no support for any other type of selectors 
        // (class, of compound) at this time.
        var elementHandler = {
            '#telecharger': function (element, renderer) {
              return true;
            }
          };
        margins = {
            top: 70,
            bottom: 60,
            left: 60,
            width: 522
        };

        pdf.setProperties({
            title: 'Liste des élèves avec leurs mot de passe'});
        // all coords and widths are in jsPDF instance's declared units
        // 'inches' in this case
        pdf.fromHTML(
            source, // HTML string or DOM elem ref.
            margins.left, // x coord
            margins.top, { // y coord
                'width': margins.width, // max width of content on PDF
                'elementHandlers': elementHandler
            },
            function (dispose) {
                pdf.save('Acces_nouvel_utilisateur_ceasy.pdf');
             }, margins
        );
            
        
       
        
 
    });
});