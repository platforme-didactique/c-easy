$(document).ready(function() {        
    $('#partieB').hide();
    $('#suivantB').hide();
    $('#precedentA').hide();
    $('#precedentB').hide();
    $('#partieC').hide();
    $('#ajoutExercice').hide();

   $('#suivantA').click(function(e){
       $("#partieA").hide();
       $('#suivantA').hide();
       $("#partieB").show();
       $('#suivantB').show();
       $('#precedentA').show();
   });

   $('#precedentA').click(function(e){
       $("#partieA").show();
       $('#suivantA').show();
       $("#partieB").hide();
       $('#suivantB').hide();
       $('#precedentA').hide();
   });

   $('#suivantB').click(function(e){
       $("#partieB").hide();
       $('#suivantB').hide();
       $('#precedentA').hide();
       $("#partieC").show();
       $('#ajoutExercice').show();
       $('#precedentB').show();
   });

   $('#precedentB').click(function(e){
       $("#partieA").hide();
       $('#suivantA').hide();
       $("#partieB").show();
       $('#suivantB').show();
       $('#precedentA').show();
       $('#precedentB').hide();
       $('#partieC').hide();
       $('#ajoutExercice').hide();
   });
});