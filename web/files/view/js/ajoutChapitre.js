$(document).ready(function () {

    $('#trumbowyg-editor').trumbowyg({
        lang: 'fr',
        btns: [
            ['viewHTML'],
            ['formatting'],
            ['strong', 'em', 'underline'],
            ['preformatted', 'highlight'],
            ['horizontalRule', 'link','insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['fullscreen']
        ]
    });

    $('#ajoutChapitre').click(function (e) {
        e.preventDefault();

        var nomChap = $("#chap").val();
        var aide = $('#trumbowyg-editor').trumbowyg('html');
        if ($("#visible").is(':checked')) {
            var visible = 1;
        }
        else {
            var visible = 0;
        }
        $.ajax({
            type: "POST",
            url: "view/Chapitre/valideAjoutChapitre.php",
            dataType: "json",
            data: { nomChapitre: nomChap, aide: aide, visible: visible },
            success: function (data) {
                if (data.code == "200") {
                    window.location = "./index.php?controller=utilisateur&action=dashboard";
                } else {
                    $(".display-error").html("<ul>" + data.msg + "</ul>");
                    $(".display-error").css("display", "block");
                }
            },
        });
    });
});