$(document).ready(function () {

    var i = 1;

    var editor = ace.edit("editor");
    editor.setOptions({
        selectionStyle: "text",
        highlightSelectedWord: true,
        fontSize: 15,
        mode: "ace/mode/c_cpp",
        theme: "ace/theme/monokai"
    });

    var editor2 = ace.edit("editor2");
    editor2.setOptions({
        selectionStyle: "text",
        highlightSelectedWord: true,
        fontSize: 15,
        mode: "ace/mode/c_cpp",
        theme: "ace/theme/monokai"
    });

    var editor3 = ace.edit("editor3");
    editor3.setOptions({
        selectionStyle: "text",
        highlightSelectedWord: true,
        fontSize: 15,
        mode: "ace/mode/c_cpp",
        theme: "ace/theme/monokai"
    });

    $('#trumbowyg-editor').trumbowyg({
        lang: 'fr',
        btns: [
            ['viewHTML'],
            ['formatting'],
            ['strong', 'em', 'underline'],
            ['horizontalRule', 'link','insertImage'],
            ['preformatted', 'highlight'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['fullscreen']
        ]
    });

    $('#ajoutExercice').click(function (e) {
        var ES = {

        };

        for (var j = 1; j <= i; j++) {
            var entree = $('#entree' + j).val();
            var sortie = $('#sortie' + j).val();

            if (entree != '' && sortie != '') {
                ES['es' + j] = {
                    entree: $('#entree' + j).val(),
                    sortie: $('#sortie' + j).val()
                }
            }

        }
        e.preventDefault();
        var code = editor2.getSession().getValue();
        var nomExo = $("#nomExo").val();
        var consigne = $('#trumbowyg-editor').trumbowyg('html');
        console.log(consigne);
        var chapitre = $('#chapitre option:selected').text();
        var dependance = editor.getSession().getValue();
        var correction = editor3.getSession().getValue();
        if ($("#visible").is(':checked')) {
            var visible = 1;
        }
        else {
            var visible = 0;
        }

        if (JSON.stringify(ES) != "{}") {
            $.ajax({
                type: "POST",
                url: "view/Exercice/valideAjoutExercice.php",
                dataType: "json",
                data: {
                    nomExo: nomExo,
                    consigne: consigne,
                    titreChap: chapitre,
                    visible: visible,
                    dependance: dependance,
                    code: code,
                    ES: JSON.stringify(ES),
                    correction: correction
                },
                success: function (data) {
                    if (data.code == "200") {
                        window.location = "./index.php?controller=utilisateur&action=dashboard";
                    } else {
                        $(".display-error").html("<ul>" + data.msg + "</ul>");
                        $(".display-error").css("display", "block");
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        } else {
            $(".display-error").html("<ul><li>Veuillez écrire au moins un jeu de test</li></ul>");
            $(".display-error").css("display", "block");
        }
    });

    $('#addES').click(function (e) {
        var original = document.getElementById('ES' + i);
        var clone = original.cloneNode(true);
        clone.id = "ES" + ++i;

        original.parentNode.appendChild(clone);
        var cloneE = $('#ES' + i).children().eq(0);
        cloneE.attr('name', "entree" + i);
        cloneE.attr('id', "entree" + i);
        $('#ES' + i).children().val('');
        var cloneS = $('#ES' + i).children().eq(1);
        cloneS.attr('name', "sortie" + i);
        cloneS.attr('id', "sortie" + i);
        cloneE.focus();
        //clone.onclick = addES; 
    });

});