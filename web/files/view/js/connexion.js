$(document).ready(function() {

    $('#submit').click(function(e){
      e.preventDefault();

      var email = $("#email").val();
      var password = $("#password").val();

      $.ajax({
          type: "POST",
          url: "view/Utilisateur/valideConnexion.php",
          dataType: "json",
          data: {email:email, password:password},
          success : function(data){
              if (data.code == "200"){
                window.location = "./index.php?controller=utilisateur&action=accueil";
              } else {
                  $(".display-error").html("<ul>"+data.msg+"</ul>");
                  $(".display-error").css("display","block");
              }
          }
      });
    });
});