function supprimerChapitre(id){

    if (window.confirm("Voulez vous vraiment supprimer ce chapitre ?")) { 
        $.ajax({
            type: "POST",
            url: "view/Chapitre/supprimerChapitre.php",
            dataType: "json",
            data: {id:id},
            success : function(data){
                location.reload(); 
            }
        });
    }
};

function supprimerPromo(id){

    if (window.confirm("Voulez vous vraiment supprimer cette promotion ?")) { 
        $.ajax({
            type: "POST",
            url: "view/Utilisateur/supprimerPromo.php",
            dataType: "json",
            data: {id:id},
            success : function(data){
                location.reload(); 
            }
        });
    }
};

function supprimerExo(idChap, id){

    if (window.confirm("Voulez vous vraiment supprimer cet exercice ?")) { 
        $.ajax({
            type: "POST",
            url: "view/Exercice/supprimerExercice.php",
            dataType: "json",
            data: {idChap:idChap, id:id},
            success : function(data){
                location.reload(); 
            }
        });
    }
};