$(document).ready(function () {

    var i = $('#lesES').find('div').length;

    var editor = ace.edit("editor");
    editor.setOptions({
        selectionStyle: "text",
        highlightSelectedWord: true,
        fontSize: 15,
        mode: "ace/mode/c_cpp",
        theme: "ace/theme/monokai",
    });

    var editor2 = ace.edit("editor2");
    editor2.setOptions({
        selectionStyle: "text",
        highlightSelectedWord: true,
        fontSize: 15,
        mode: "ace/mode/c_cpp",
        theme: "ace/theme/monokai"
    });

    var editor3 = ace.edit("editor3");
    editor3.setOptions({
        selectionStyle: "text",
        highlightSelectedWord: true,
        fontSize: 15,
        mode: "ace/mode/c_cpp",
        theme: "ace/theme/monokai"
    });


    var recuptrumbowyg = document.getElementById('trumbowyg-editor').innerHTML;
    $('#trumbowyg-editor').trumbowyg({
        lang: 'fr',
        btns: [
            ['viewHTML'],
            ['formatting'],
            ['strong', 'em', 'underline'],
            ['horizontalRule', 'link','insertImage'],
            ['preformatted', 'highlight'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['fullscreen']
        ]
    });
    $('#trumbowyg-editor').trumbowyg('html', recuptrumbowyg);

    $('#addES').click(function (e) {
        var original = document.getElementById('ES' + i);
        var clone = original.cloneNode(true);
        clone.id = "ES" + ++i;

        original.parentNode.appendChild(clone);
        var cloneE = $('#ES' + i).children().eq(0);
        cloneE.attr('name', "entree" + i);
        cloneE.attr('id', "entree" + i);
        $('#ES' + i).children().val('');
        var cloneS = $('#ES' + i).children().eq(1);
        cloneS.attr('name', "sortie" + i);
        cloneS.attr('id', "sortie" + i);
        cloneE.focus();
        //clone.onclick = addES; 
    });
});

function modifierExo(id_chap, id) {
    var i = $('#lesES').find('div').length;
    var editor = ace.edit("editor");
    var editor2 = ace.edit("editor2");
    var editor3 = ace.edit("editor3");
    var ES = {

    };

    for (var j = 1; j <= i; j++) {
        var entree = $('#entree' + j).val();
        var sortie = $('#sortie' + j).val();

        if (entree != '' && sortie != '') {
            ES['es' + j] = {
                entree: $('#entree' + j).val(),
                sortie: $('#sortie' + j).val()
            }
        }

    }
    var code = editor2.getSession().getValue();
    var nomExo = $("#nomExo").val();
    var consigne = $('#trumbowyg-editor').trumbowyg('html');
    var chapitre = $('#chapitre option:selected').text();
    var dependance = editor.getSession().getValue();
    var correction = editor3.getSession().getValue();
    if ($("#visible").is(':checked')) {
        var visible = 1;
    }
    else {
        var visible = 0;
    }

    if (JSON.stringify(ES) != "{}") {
        $.ajax({
            type: "POST",
            url: "view/Exercice/valideModifierExercice.php",
            dataType: "json",
            data: {
                id_chap: id_chap,
                id: id,
                nomExo: nomExo,
                consigne: consigne,
                titreChap: chapitre,
                visible: visible,
                dependance: dependance,
                code: code,
                ES: JSON.stringify(ES),
                correction: correction
            },
            success: function (data) {
                console.log(data);
                if (data.code == "200") {
                    window.location = "./index.php?controller=utilisateur&action=dashboard";
                } else {
                    $(".display-error").html("<ul>" + data.msg + "</ul>");
                    $(".display-error").css("display", "block");
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    } else {
        $(".display-error").html("<ul><li>Veuillez écrire au moins un jeu de test</li></ul>");
        $(".display-error").css("display", "block");
    }
}