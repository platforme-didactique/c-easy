$(document).ready(function () {

    var recuptrumbowyg = document.getElementById('trumbowyg-editor').innerHTML;
    $('#trumbowyg-editor').trumbowyg({
        lang: 'fr',
        btns: [
            ['viewHTML'],
            ['formatting'],
            ['strong', 'em', 'underline'],
            ['horizontalRule', 'link','insertImage'],
            ['preformatted', 'highlight'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['fullscreen']
        ]
    });
    $('#trumbowyg-editor').trumbowyg('html', recuptrumbowyg);
});

function modifierChapitre(id_chap) {

    var nomChap = $("#chap").val();
    var aide = $('#trumbowyg-editor').trumbowyg('html');
    if ($("#visible").is(':checked')) {
        var visible = 1;
    }
    else {
        var visible = 0;
    }
    $.ajax({
        type: "POST",
        url: "view/Chapitre/valideModifierChapitre.php",
        dataType: "json",
        data: { nomChapitre: nomChap, aide: aide, visible: visible, id_chap: id_chap },
        success: function (data) {
            if (data.code == "200") {
                window.location = "./index.php?controller=utilisateur&action=dashboard";
            } else {
                $(".display-error").html("<ul>" + data.msg + "</ul>");
                $(".display-error").css("display", "block");
            }
        }, error: function (data) { console.log(data); }

    });
}