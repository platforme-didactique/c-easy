<?php
include_once "models/Exercice.php";
include_once "models/Realise.php";
include_once "models/Chapitre.php";

class ControllerExercice {

	protected static $object;

    public static function execute($conn) {
        try{

            $exercice = Exercice::getExerciceDB($conn, $_GET['idChapitre'], $_GET['idExo']);

            if($exercice->visible != 1) throw new Exception("Exercice invisible");

            $realise_existe = Realise::realiseExisteByID($conn,$_GET['idExo'],$_GET['idChapitre'],unserialize($_SESSION['utilisateur'])->id);

            if(!$realise_existe){
                Realise::createRealiseDB($conn,$_GET['idExo'],$_GET['idChapitre'],unserialize($_SESSION['utilisateur'])->id);
            }
            else{
                $realise=Realise::getRealiseDB($conn, $_GET['idExo'],$_GET['idChapitre'],unserialize($_SESSION['utilisateur'])->id);
            }
            require_once File::build_path(array("view", "Chapitre", "chapitre.php"));
            require_once File::build_path(array("view", "Exercice" ,"exercice.php"));  //"redirige" vers la vue
        }
        catch(Exception $e){
            require_once File::build_path(array("view","Autre","error.php"));
        }
        
    }


    public static function ajoutexercice($conn) {
        if(unserialize($_SESSION['utilisateur'])->type == 1){
            header('Location: index.php');
        }
        else {
            $chapitre=Chapitre::getAllChapitreDB($conn);
            require_once File::build_path(array("view", "Exercice" ,"ajout_exercice.php"));
        }

    }
    
    public static function modifierexercice($conn){
        if(unserialize($_SESSION['utilisateur'])->type == 1){
            header('Location: index.php');
        }
        else {
            
            if( isset($_GET['idchap']) && Chapitre::chapitreExisteByID($conn, $_GET['idchap']) && isset( $_GET['id']) && Exercice::exerciceExisteByID($conn, $_GET['idchap'], $_GET['id']) ){
                try{
                    $allchapitre=Chapitre::getAllChapitreDB($conn);
                    $chapitre=Chapitre::getChapitreDB($conn,$_GET['idchap']);
                    $exercice=Exercice::getExerciceDB($conn,$_GET['idchap'],$_GET['id']);
                    $lesTests=JeuTest::getJeuTestDB($conn, $_GET['idchap'],$_GET['id']);
                    require_once File::build_path(array("view", "Exercice", "modifier_exercice.php"));
                }
                catch(Exception $e){
                    require_once File::build_path(array("view","Autre","error.php"));
                }
            }
            else{
                require_once File::build_path(array("view","Autre","error.php"));
            }

        }
    }
    
}
?>