<?php
include_once "models/Utilisateur.php";
include_once "models/Promo.php";
class ControllerUtilisateur {

	protected static $object;

    public static function connexion($conn) {
        require_once File::build_path(array("view", "Utilisateur", "connexion.php"));

    }

    public static function accueil($conn) {
        if( isset($_POST['email']) && isset($_POST['password'])  ){
            $_SESSION['utilisateur'] = serialize( Utilisateur::verificationConnection($conn, $_POST['email'],$_POST['password']) );
        }
        require_once File::build_path(array("view", "Chapitre", "chapitre.php"));
        require_once File::build_path(array("view", "Utilisateur", "accueil.php"));
    }

    public static function dashboard($conn){
        if(unserialize($_SESSION['utilisateur'])->type == 1){
            header('Location: index.php');
        }
        else {
            require_once File::build_path(array("view", "Utilisateur", "dashboard.php"));
        }
    }

    public static function credits($conn){
        require_once File::build_path(array("view", "Autre", "credits.php"));
    }

    public static function ajoutpromo($conn){
        if(unserialize($_SESSION['utilisateur'])->type == 1){
            header('Location: index.php');
            
        }else{
            require_once File::build_path(array("view", "Utilisateur", "ajout_promo.php"));
        }
    }
    
    public static function profil($conn){
        if(unserialize($_SESSION['utilisateur'])->type == 1){
            require_once File::build_path(array("view", "Chapitre", "chapitre.php"));
        }
        require_once File::build_path(array("view", "Utilisateur", "profil.php"));
    }

    public static function ajouteutilisateur($conn){
        if(unserialize($_SESSION['utilisateur'])->type == 1){
            header('Location: index.php');
            
        }else{
            try{
                require_once File::build_path(array("view", "Utilisateur", "ajout_utilisateur.php"));
            }catch(Exception $e){
                print_r($r->getMessage());
            }
        }
        
        
    }

    public static function nextajout($conn){
        if(unserialize($_SESSION['utilisateur'])->type == 1){
            require_once File::build_path(array("view", "Chapitre", "chapitre.php"));
            
        }
        require_once File::build_path(array("view", "Utilisateur", "affiche_mdp.php"));
            
        
        
        
    }
}
?>