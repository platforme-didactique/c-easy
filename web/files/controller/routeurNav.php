<?php

    require_once File::build_path(array("controller", "controllerChapitre.php"));
    require_once File::build_path(array("controller", "controllerExercice.php"));
    require_once File::build_path(array("controller", "controllerUtilisateur.php"));
            
    if (isset($_SESSION['utilisateur']) ){
        
        if (unserialize($_SESSION['utilisateur'])->type == 1){
            require_once File::build_path(array("view","Navbar","eleve.php"));
        }
        else{
            require_once File::build_path(array("view","Navbar","admin.php"));
        }
    }
?>