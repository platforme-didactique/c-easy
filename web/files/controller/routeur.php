<?php

    require_once File::build_path(array("controller", "controllerChapitre.php"));
    require_once File::build_path(array("controller", "controllerExercice.php"));
    require_once File::build_path(array("controller", "controllerUtilisateur.php"));
            
    if ( !isset($_GET['action']) || !isset($_GET['controller']) || !isset($_SESSION['utilisateur']) ){
        $controller = 'utilisateur';

        if(isset($_GET['action']) && $_GET['action'] == "credits" ){
            $action = $_GET['action'];
        }
        else{
            if( !isset($_SESSION['utilisateur']) ){
                $action = 'connexion';
            }
            else{
                $action = 'accueil';
            }
        }
        
    }
    else{
        $controller = $_GET['controller'];
        $action = $_GET['action'];
    }

    $ok = false;

    $controllerclass="controller".ucfirst($controller);
    if ($ok = class_exists($controllerclass) ){
        $class_methods = get_class_methods($controllerclass);

        if ($ok = in_array($action, $class_methods) ){
            $controllerclass::$action($conn); // Appel de la méthode statique $action du controlleur $controllerclass
        }
    }

    if(!$ok){
        require_once File::build_path(array("view","Autre","error.php"));
    }
?>