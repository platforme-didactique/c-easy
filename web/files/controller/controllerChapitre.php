<?php
include_once "models/Chapitre.php";
class ControllerChapitre {

	protected static $object;

	public static function ajoutchapitre($conn){
        if(unserialize($_SESSION['utilisateur'])->type == 1){
            header('Location: index.php');
        }
        else {
            require_once File::build_path(array("view", "Chapitre", "ajout_chapitre.php"));
        }
    }   

    public static function modifierchapitre($conn){
        if(unserialize($_SESSION['utilisateur'])->type == 1){
            header('Location: index.php');
        }
        else {
            
            if( isset($_GET['id']) && Chapitre::chapitreExisteByID($conn, $_GET['id']) ){
                $chapitre=Chapitre::getChapitreDB($conn,$_GET['id']);
                require_once File::build_path(array("view", "Chapitre", "modifier_chapitre.php"));
            }
            else{
                require_once File::build_path(array("view","Autre","error.php"));
            }

        }
    }

}
?>